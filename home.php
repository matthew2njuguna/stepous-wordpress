<?php
/**
 * Template Name: Home Page Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package beleen
 */

get_header(); ?>

<?php 
$upcomingCat = 17;
$causesCat = 5;
$volsCat = 12;
?>

        <!-- Hero Slider
        ================================================== -->
        <div class="hero-slider-wrapper">
        
            <div class="hero-slider house">
        
                <!-- Slide #0 -->
                <div class="hero-slider__item hero-slider__item--img1" style="background-image: url(assets/images/homebanner.jpeg);">
        
                    <div class="container hero-slider__item-container">
                        <div class="row">
                            <div class="col-md-8 offset-md-2">
                                <!-- Post Meta - Top -->
                                <div class="post__meta-block post__meta-block--top">
        
                                    <!-- Post Category -->
                                    <div class="post__category">
                                        <span class="label posts__cat-label">Africa</span>
                                    </div>
                                    <!-- Post Category / End -->
        
                                    <!-- Post Title -->
                                    <h1 class="page-heading__title"><a href="_soccer_blog-post-1.html">Discover The Highlights Of Africa</h1>
                                        <p class=" text-white">Talk to us, we’ll show you Africa.</p>
                                    <!-- Post Title / End -->
        
                                    <!-- Post Meta Info -->
                                    <ul class="post__meta meta">
                                            <li class="meta__item"><a href="#" class="btn btn-primary btn-sm btn-icon-right ">Enquire Now <i class="icon-envelope-letter"></i></a></li>
                                            <li class="meta__item">or</li>
                                            <li class="meta__item"><a href="#" class="btn btn-secondary btn-sm  btn-icon-right ">Contact Us <i class="icon-earphones-alt"></i></a></li>
                                    </ul>
                                    <!-- Post Meta Info / End -->
        
        
                                </div>
                                <!-- Post Meta - Top / End -->
                            </div>
                        </div>
                    </div>
        
                </div>
                <!-- Slide #0 / End -->
        
                <!-- Slide #0 -->
                <div class="hero-slider__item hero-slider__item--img2" style="background-image: url(assets/images/mara.jpg);">
        
                    <div class="container hero-slider__item-container">
                        <div class="row">
                            <div class="col-md-8 offset-md-2">
                                <!-- Post Meta - Top -->
                                <div class="post__meta-block post__meta-block--top">
        
                                    <!-- Post Category -->
                                    <div class="post__category">
                                        <span class="label posts__cat-label">Masaai Mara</span>
                                    </div>
                                    <!-- Post Category / End -->
        
                                    <!-- Post Title -->
                                    <h1 class="page-heading__title"><a href="_soccer_blog-post-1.html">The Eighth Wonder Of The World </h1>
                                        <p class=" text-white">Talk to us, we’ll show you Africa.</p>
                                    <!-- Post Title / End -->
        
                                    <!-- Post Meta Info -->
                                    <ul class="post__meta meta">
                                        <li class="meta__item"><a href="#" class="btn btn-primary btn-sm btn-icon-right ">Enquire Now <i class="icon-envelope-letter"></i></a></li>
                                        <li class="meta__item">or</li>
                                        <li class="meta__item"><a href="#" class="btn btn-secondary btn-sm  btn-icon-right ">Contact Us <i class="icon-earphones-alt"></i></a></li>
                                    </ul>
                                    <!-- Post Meta Info / End -->
        
        
                                </div>
                                <!-- Post Meta - Top / End -->
                            </div>
                        </div>
                    </div>
        
                </div>
                <!-- Slide #0 / End -->
            </div>
        
            <div class="hero-slider-thumbs-wrapper">
                <div class="container">
                    <div class="hero-slider-thumbs posts posts--simple-list">
        
                        <div class="hero-slider-thumbs__item">
                            <div class="posts__item posts__item--category-1">
                                <div class="posts__inner">
                                    <div class="posts__cat">
                                        <span class="label posts__cat-label">Africa</span>
                                    </div>
                                    <h6 class="posts__title">Discover The Highlights Of Africa</h6>
                                    <time class="posts__date">Talk to us, we’ll show you Africa.</time>
                                </div>
                            </div>
                        </div>
                        <div class="hero-slider-thumbs__item">
                            <div class="posts__item posts__item--category-2">
                                <div class="posts__inner">
                                    <div class="posts__cat">
                                        <span class="label posts__cat-label">Masaai Mara</span>
                                    </div>
                                    <h6 class="posts__title">Discover The Highlights Of Africa</h6>
                                    <time class="posts__date">Talk to us, we’ll show you Africa.</time>
                                </div>
                            </div>
                        </div>
        
                    </div>
                </div>
            </div>
        
        
        </div>

        

            
        <!-- Content
        ================================================== -->
        <div class="site-content">

        
                        <div class="row introw bg-white mb-5">
                <div class="content col-md-12">
                <div class="main-news-banner__inner container">
                        <div class="posts posts--simple-list posts--simple-list--lg float-left col-md-5">
                            <div class="posts__item intro1 ">
                                <div class="posts__inner">
                                    <h4>10+ years of experience in the Tourism Industry, our Company rocks at finding the best 
                                        holiday properties and recommendations based on each clients needs.</h4> 
                                </div>
                            </div>
                        </div>
                        <div class="posts posts--simple-list posts--simple-list--lg float-right col-md-4">
                            <div class="posts__item intro2">
                                <div class="posts__inner">
                                    <q>We pride ourselves in offering select 4 x 4 safari packages, tailor made 
                                    safaris both Air and ground packages. Client satisfaction will always be our pride and
                                     our Team of Travel planners have great knowledge in Wildlife and the destination they are
                                      selling to you! Our warm personal hospitality, uncompromising high quality service has been the hallmark of our continued success. 
                                    Feel free to share with us your ideas of your ideal holiday and we will be as close as possible to make your dream come true!</q>
                                    <p class="text-uppercase small col-12 pt-5"><b class="text-capitalize">Peter K. & Jane M.</b> <br> Directors - Beleen Safaris</p>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="posts__more row">
                            <a href="#" class="btn btn-inverse btn-sm btn-outline btn-icon-right btn-condensed">Learn more about us</a>
                        </div> 
                    </div>
                    
                <div class="post__meta-block post__meta-block--top col-md-12 p-0">
        
        
                <!-- Post Author -->
                <div class="post-author mb-5">
                    <div class="post-author__info">
                        <h2 class="">Our Recommendations</h2>
                        <span class="">Plan your holiday with these popular favourites...</span>
                    </div>
                </div>
                <!-- Post Author / End -->
    
                    </div>
                    <div class="container">
                <div class="recomends row">
            
                        <div class="col-md-3">
                                <!-- Player Card # -->
                                <div class="post-grid__item col-sm-12 team-roster__item ">
                                    <div class="posts__item posts__item--card posts__item--category-1 card">
                                        <figure class="posts__thumb">
                                            <div class="posts__cat">
                                                <span class="label posts__cat-label">KENYA</span>
                                                <span class="label posts__cat-label">UGANDA</span>
                                            </div>
                                            <a href="#"><img src="assets/images/Madagascar.jpeg" alt=""></a>
                                            <span class="angled"></span>
                                        </figure>
                                        <div class="posts__inner card__content">
                                            <a href="#" class="posts__cta"><span class="expan hids">View details</span></a>
                                            <h6 class="posts__title"><a href="#">Captivating Uganda - Kenya Safari</a></h6>
                                    
                                            <time datetime="2016-08-23" class="posts__date">9 days</time>
                                            <ol class="breadcrumb">
                                                    <li class="breadcrumb-item"><a href="">Entebe</a></li>
                                                    <li class="breadcrumb-item"><a href="">Gorilla trecking</a></li>
                                                    <li class="breadcrumb-item">Nairobi</li>
                                                    <li class="breadcrumb-item">Masai Mara</li>
                                                </ol>
                                        </div>
                                        <footer class="posts__footer card__footer">
                                            <div class="post-author">
                                                <div class="post-author__info">
                                                        <a href="#" class="posts__nq btn btn-light hids">Please enquire</a>
                                                    <h4 class="post-author__name text-gray">
                                                            $3,000<span class=""> AV/PP</span>
                                                    </h4>
                                                </div>
                                            </div>
                                        </footer>
                                    </div>
                                </div>
                                <!-- Player Card # / End -->
                            </div>
                            <div class="col-md-3">
                                <!-- Player Card # -->
                                <div class="post-grid__item col-sm-12 team-roster__item ">
                                    <div class="posts__item posts__item--card posts__item--category-1 card">
                                        <figure class="posts__thumb">
                                            <div class="posts__cat">
                                                <span class="label posts__cat-label">TANZANIA</span>
                                                <span class="label posts__cat-label">ZANZIBAR</span>
                                            </div>
                                            <a href="#"><img src="assets/images/Beach.png" alt=""></a>
                                            <span class="angled"></span>
                                        </figure>
                                        <div class="posts__inner card__content">
                                                <a href="#" class="posts__cta"><span class="expan hids">View details</span></a>
                                            <h6 class="posts__title"><a href="#">Tanzani Safari & Zanzibar 
                                                    Beach Adventure</a></h6>
                                    
                                            <time datetime="2016-08-23" class="posts__date">9 days</time>
                                            <ol class="breadcrumb">
                                                    <li class="breadcrumb-item"><a href="">Nairobi</a></li>
                                                    <li class="breadcrumb-item"><a href="">Aberdares</a></li>
                                                    <li class="breadcrumb-item">Nakuru</li>
                                                    <li class="breadcrumb-item">Masai Mara</li>
                                                </ol>
                                        </div>
                                        <footer class="posts__footer card__footer">
                                            <div class="post-author">
                                                <div class="post-author__info">
                                                        <a href="#" class="posts__nq btn btn-light hids">Please enquire</a>
                                                    <h4 class="post-author__name">
                                                            $3,000<span class="text-gray"> /PP</span></h4>
                                                </div>
                                            </div>
                                        </footer>
                                    </div>
                                </div>
                                <!-- Player Card # / End -->
                            </div>
                            <div class="col-md-3">
                                <!-- Player Card # -->
                                <div class="post-grid__item col-sm-12 team-roster__item ">
                                    <div class="posts__item posts__item--card posts__item--category-1 card">
                                        <figure class="posts__thumb">
                                            <div class="posts__cat">
                                                <span class="label posts__cat-label">KENYA</span>
                                            </div>
                                            <a href="#"><img src="assets/images/Zambia.jpeg" alt=""></a>
                                            <span class="angled"></span>
                                        </figure>
                                        <div class="posts__inner card__content">
                                                                                        <a href="#" class="posts__cta"><span class="expan hids">View details</span></a>
                                            <h6 class="posts__title"><a href="#">Kenya Safari for 
                                                    Honeymooners
                                                    </a></h6>
                                    
                                            <time datetime="2016-08-23" class="posts__date">9 days</time>
                                            <ol class="breadcrumb">
                                                    <li class="breadcrumb-item"><a href="">Nairobi</a></li>
                                                    <li class="breadcrumb-item"><a href="">Aberdares</a></li>
                                                    <li class="breadcrumb-item">Nakuru</li>
                                                    <li class="breadcrumb-item">Masai Mara</li>
                                                </ol>
                                        </div>
                                        <footer class="posts__footer card__footer">
                                            <div class="post-author">
                                                <div class="post-author__info">
                                                        <a href="#" class="posts__nq btn btn-light hids">Please enquire</a>
                                                    <h4 class="post-author__name">
                                                            $3,000<span class="text-gray"> /PP</span></h4>
                                                </div>
                                            </div>
                                        </footer>
                                    </div>
                                </div>
                                <!-- Player Card # / End -->
                            </div>
                            <div class="col-md-3">
                                <!-- Player Card # -->
                                <div class="post-grid__item col-sm-12 team-roster__item ">
                                    <div class="posts__item posts__item--card posts__item--category-1 card">
                                        <figure class="posts__thumb">
                                            <div class="posts__cat">
                                                <span class="label posts__cat-label">KENYA</span>
                                                <span class="label posts__cat-label">UGANDA</span>
                                            </div>
                                            <a href="#"><img src="assets/images/Zambia.jpeg" alt=""></a>
                                            <span class="angled"></span>
                                        </figure>
                                        <div class="posts__inner card__content">
                                                                                        <a href="#" class="posts__cta"><span class="expan hids">View details</span></a>
                                            <h6 class="posts__title"><a href="#">Captivating Uganda - Kenya Safari</a></h6>
                                    
                                            <time datetime="2016-08-23" class="posts__date">9 days</time>
                                            <ol class="breadcrumb">
                                                    <li class="breadcrumb-item"><a href="">Entebe</a></li>
                                                    <li class="breadcrumb-item"><a href="">Gorilla trecking</a></li>
                                                    <li class="breadcrumb-item">Nairobi</li>
                                                    <li class="breadcrumb-item">Masai Mara</li>
                                                </ol>
                                        </div>
                                        <footer class="posts__footer card__footer">
                                            <div class="post-author">
                                                <div class="post-author__info">
                                                        <a href="#" class="posts__nq btn btn-light hids">Please enquire</a>
                                                    <h4 class="post-author__name">
                                                            $3,000<span class="text-gray"> /PP</span></h4>
                                                </div>
                                            </div>
                                        </footer>
                                    </div>
                                </div>
                                <!-- Player Card # / End -->
                            </div>
                            <div class="col-md-3">
                                <!-- Player Card # -->
                                <div class="post-grid__item col-sm-12 team-roster__item ">
                                    <div class="posts__item posts__item--card posts__item--category-1 card">
                                        <figure class="posts__thumb">
                                            <div class="posts__cat">
                                                <span class="label posts__cat-label">TANZANIA</span>
                                                <span class="label posts__cat-label">ZANZIBAR</span>
                                            </div>
                                            <a href="#"><img src="assets/images/Zambia.jpeg" alt=""></a>
                                            <span class="angled"></span>
                                        </figure>
                                        <div class="posts__inner card__content">
                                                                                        <a href="#" class="posts__cta"><span class="expan hids">View details</span></a>
                                            <h6 class="posts__title"><a href="#">Tanzani Safari & Zanzibar 
                                                    Beach Adventure</a></h6>
                                    
                                            <time datetime="2016-08-23" class="posts__date">9 days</time>
                                            <ol class="breadcrumb">
                                                    <li class="breadcrumb-item"><a href="">Nairobi</a></li>
                                                    <li class="breadcrumb-item"><a href="">Aberdares</a></li>
                                                    <li class="breadcrumb-item">Nakuru</li>
                                                    <li class="breadcrumb-item">Masai Mara</li>
                                                </ol>
                                        </div>
                                        <footer class="posts__footer card__footer">
                                            <div class="post-author">
                                                <div class="post-author__info">
                                                        <a href="#" class="posts__nq btn btn-light hids">Please enquire</a>
                                                    <h4 class="post-author__name">
                                                            $3,000<span class="text-gray"> /PP</span></h4>
                                                </div>
                                            </div>
                                        </footer>
                                    </div>
                                </div>
                                <!-- Player Card # / End -->
                            </div>
                            <div class="col-md-3">
                                <!-- Player Card # -->
                                <div class="post-grid__item col-sm-12 team-roster__item ">
                                    <div class="posts__item posts__item--card posts__item--category-1 card">
                                        <figure class="posts__thumb">
                                            <div class="posts__cat">
                                                <span class="label posts__cat-label">KENYA</span>
                                            </div>
                                            <a href="#"><img src="assets/images/Seychelles.jpg" alt=""></a>
                                            <span class="angled"></span>
                                        </figure>
                                        <div class="posts__inner card__content">
                                                                                        <a href="#" class="posts__cta"><span class="expan hids">View details</span></a>
                                            <h6 class="posts__title"><a href="#">Kenya Safari for 
                                                    Honeymooners
                                                    </a></h6>
                                    
                                            <time datetime="2016-08-23" class="posts__date">9 days</time>
                                            <ol class="breadcrumb">
                                                    <li class="breadcrumb-item"><a href="">Nairobi</a></li>
                                                    <li class="breadcrumb-item"><a href="">Aberdares</a></li>
                                                    <li class="breadcrumb-item">Nakuru</li>
                                                    <li class="breadcrumb-item">Masai Mara</li>
                                                </ol>
                                        </div>
                                        <footer class="posts__footer card__footer">
                                            <div class="post-author">
                                                <div class="post-author__info">
                                                        <a href="#" class="posts__nq btn btn-light hids">Please enquire</a>
                                                    <h4 class="post-author__name">
                                                            $3,000<span class="text-gray"> /PP</span></h4>
                                                </div>
                                            </div>
                                        </footer>
                                    </div>
                                </div>
                                <!-- Player Card # / End -->
                            </div>
                    </div>
                    <div class="posts__more row">
                        <a href="#" class="btn btn-inverse btn-sm btn-outline btn-icon-right btn-condensed">View More Tour ideas </a>
                    </div>
                </div>



                    <div class=" widget-tabbed container" >
                    
                <!-- Post Author -->
                <div class="post-author mt-5 mb-5 text-center">
                        <div class="post-author__info">
                            <h2 class="">Find Your Perfect Holiday</h2>
                            <span class="">We'll help you plan your perfect holiday...</span>
                        </div>
                    </div>
                    <!-- Post Author / End -->
                            <div class="widget__content card__content">
                                <div class="widget-tabbed__tabs">
                                    <!-- Widget Tabs -->
                                    <ul class="nav nav-tabs widget-tabbed__nav" role="tablist">
                                        <li class="nav-item"><a href="#widget-tabbed-sm-newest" class="nav-link active" aria-controls="widget-tabbed-sm-newest" role="tab" data-toggle="tab">Top Destinations</a></li>
                                        <li class="nav-item"><a href="#widget-tabbed-sm-commented" class="nav-link" aria-controls="widget-tabbed-sm-commented" role="tab" data-toggle="tab">Top Experiences</a></li>
                                    </ul>
                        
                                    <!-- Widget Tab panes -->
                                    <div class="tab-content widget-tabbed__tab-content">
                                        <!-- Newest -->
                                        <div role="tabpanel" class="tab-pane fade show active" id="widget-tabbed-sm-newest">
                                        
                                            <div class="perfect row">
                                                <div class="col-md-3">
                                                    <!-- Player Card # -->
                                                    <div class="post-grid__item col-sm-12 team-roster__item " >
                                                        <div class="posts__item posts__item--card posts__item--quote" style="background-image: url(assets/images/Laikipia.jpg);">
                                                            <div class="card__content">
                                                                <blockquote class="blockquote blockquote--card">
                                                                    
                                                                    <footer class="blockquote__footer">
                                                                        <cite class="blockquote__cite">
                                                                            <span class="blockquote__author-name">Markus Keymore</span>
                                                                            <span class="blockquote__author-info">Beleen Traveller</span>
                                                                        </cite>
                                                                    </footer>
                                                                </blockquote>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Player Card # / End -->
                                                </div>
                                                <div class="col-md-3">
                                                    <!-- Player Card # -->
                                                    <div class="post-grid__item col-sm-12 team-roster__item " >
                                                        <div class="posts__item posts__item--card posts__item--quote" style="background-image: url(assets/images/Laikipia.jpg);">
                                                            <div class="card__content">
                                                                <blockquote class="blockquote blockquote--card">
                                                                    
                                                                    <footer class="blockquote__footer">
                                                                        <cite class="blockquote__cite">
                                                                            <span class="blockquote__author-name">Markus Keymore</span>
                                                                            <span class="blockquote__author-info">Beleen Traveller</span>
                                                                        </cite>
                                                                    </footer>
                                                                </blockquote>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Player Card # / End -->
                                                </div>
                                                <div class="col-md-4">
                                                    <!-- Player Card # -->
                                                    <div class="post-grid__item col-sm-12 team-roster__item " >
                                                        <div class="posts__item posts__item--card posts__item--quote" style="background-image: url(assets/images/Laikipia.jpg);">
                                                            <div class="card__content">
                                                                <blockquote class="blockquote blockquote--card">
                                                                    
                                                                    <footer class="blockquote__footer">
                                                                        <cite class="blockquote__cite">
                                                                            <span class="blockquote__author-name">Markus Keymore</span>
                                                                            <span class="blockquote__author-info">Beleen Traveller</span>
                                                                        </cite>
                                                                    </footer>
                                                                </blockquote>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Player Card # / End -->
                                                </div>
                                                <div class="col-md-3">
                                                    <!-- Player Card # -->
                                                    <div class="post-grid__item col-sm-12 team-roster__item " >
                                                        <div class="posts__item posts__item--card posts__item--quote" style="background-image: url(assets/images/Laikipia.jpg);">
                                                            <div class="card__content">
                                                                <blockquote class="blockquote blockquote--card">
                                                                    
                                                                    <footer class="blockquote__footer">
                                                                        <cite class="blockquote__cite">
                                                                            <span class="blockquote__author-name">Markus Keymore</span>
                                                                            <span class="blockquote__author-info">Beleen Traveller</span>
                                                                        </cite>
                                                                    </footer>
                                                                </blockquote>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Player Card # / End -->
                                                </div>
                                                <div class="col-md-3">
                                                    <!-- Player Card # -->
                                                    <div class="post-grid__item col-sm-12 team-roster__item " >
                                                        <div class="posts__item posts__item--card posts__item--quote" style="background-image: url(assets/images/Laikipia.jpg);">
                                                            <div class="card__content">
                                                                <blockquote class="blockquote blockquote--card">
                                                                    
                                                                    <footer class="blockquote__footer">
                                                                        <cite class="blockquote__cite">
                                                                            <span class="blockquote__author-name">Markus Keymore</span>
                                                                            <span class="blockquote__author-info">Beleen Traveller</span>
                                                                        </cite>
                                                                    </footer>
                                                                </blockquote>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Player Card # / End -->
                                                </div>
                                                <div class="col-md-3">
                                                    <!-- Player Card # -->
                                                    <div class="post-grid__item col-sm-12 team-roster__item " >
                                                        <div class="posts__item posts__item--card posts__item--quote" style="background-image: url(assets/images/Laikipia.jpg);">
                                                            <div class="card__content">
                                                                <blockquote class="blockquote blockquote--card">
                                                                    
                                                                    <footer class="blockquote__footer">
                                                                        <cite class="blockquote__cite">
                                                                            <span class="blockquote__author-name">Markus Keymore</span>
                                                                            <span class="blockquote__author-info">Beleen Traveller</span>
                                                                        </cite>
                                                                    </footer>
                                                                </blockquote>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!-- Player Card # / End -->
                                                </div>
                                            </div>
                                            
                                        </div>
                                        <!-- Commented -->
                                        <div role="tabpanel" class="tab-pane fade" id="widget-tabbed-sm-commented">
                                        
                                                <div class="perfect2 row">
                                                        <div class="col-md-3">
                                                            <!-- Player Card # -->
                                                            <div class="post-grid__item col-sm-12 team-roster__item " >
                                                                <div class="posts__item posts__item--card posts__item--quote" style="background-image: url(assets/images/Laikipia.jpg);">
                                                                    <div class="card__content">
                                                                        <blockquote class="blockquote blockquote--card">
                                                                            
                                                                            <footer class="blockquote__footer">
                                                                                <cite class="blockquote__cite">
                                                                                    <span class="blockquote__author-name">Markus Keymore</span>
                                                                                    <span class="blockquote__author-info">Beleen Traveller</span>
                                                                                </cite>
                                                                            </footer>
                                                                        </blockquote>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- Player Card # / End -->
                                                        </div>
                                                        <div class="col-md-3">
                                                            <!-- Player Card # -->
                                                            <div class="post-grid__item col-sm-12 team-roster__item " >
                                                                <div class="posts__item posts__item--card posts__item--quote" style="background-image: url(assets/images/Laikipia.jpg);">
                                                                    <div class="card__content">
                                                                        <blockquote class="blockquote blockquote--card">
                                                                            
                                                                            <footer class="blockquote__footer">
                                                                                <cite class="blockquote__cite">
                                                                                    <span class="blockquote__author-name">Markus Keymore</span>
                                                                                    <span class="blockquote__author-info">Beleen Traveller</span>
                                                                                </cite>
                                                                            </footer>
                                                                        </blockquote>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- Player Card # / End -->
                                                        </div>
                                                        <div class="col-md-4">
                                                            <!-- Player Card # -->
                                                            <div class="post-grid__item col-sm-12 team-roster__item " >
                                                                <div class="posts__item posts__item--card posts__item--quote" style="background-image: url(assets/images/Laikipia.jpg);">
                                                                    <div class="card__content">
                                                                        <blockquote class="blockquote blockquote--card">
                                                                            
                                                                            <footer class="blockquote__footer">
                                                                                <cite class="blockquote__cite">
                                                                                    <span class="blockquote__author-name">Markus Keymore</span>
                                                                                    <span class="blockquote__author-info">Beleen Traveller</span>
                                                                                </cite>
                                                                            </footer>
                                                                        </blockquote>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- Player Card # / End -->
                                                        </div>
                                                        <div class="col-md-3">
                                                            <!-- Player Card # -->
                                                            <div class="post-grid__item col-sm-12 team-roster__item " >
                                                                <div class="posts__item posts__item--card posts__item--quote" style="background-image: url(assets/images/Laikipia.jpg);">
                                                                    <div class="card__content">
                                                                        <blockquote class="blockquote blockquote--card">
                                                                            
                                                                            <footer class="blockquote__footer">
                                                                                <cite class="blockquote__cite">
                                                                                    <span class="blockquote__author-name">Markus Keymore</span>
                                                                                    <span class="blockquote__author-info">Beleen Traveller</span>
                                                                                </cite>
                                                                            </footer>
                                                                        </blockquote>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- Player Card # / End -->
                                                        </div>
                                                        <div class="col-md-3">
                                                            <!-- Player Card # -->
                                                            <div class="post-grid__item col-sm-12 team-roster__item " >
                                                                <div class="posts__item posts__item--card posts__item--quote" style="background-image: url(assets/images/Laikipia.jpg);">
                                                                    <div class="card__content">
                                                                        <blockquote class="blockquote blockquote--card">
                                                                            
                                                                            <footer class="blockquote__footer">
                                                                                <cite class="blockquote__cite">
                                                                                    <span class="blockquote__author-name">Markus Keymore</span>
                                                                                    <span class="blockquote__author-info">Beleen Traveller</span>
                                                                                </cite>
                                                                            </footer>
                                                                        </blockquote>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- Player Card # / End -->
                                                        </div>
                                                        <div class="col-md-3">
                                                            <!-- Player Card # -->
                                                            <div class="post-grid__item col-sm-12 team-roster__item " >
                                                                <div class="posts__item posts__item--card posts__item--quote" style="background-image: url(assets/images/Laikipia.jpg);">
                                                                    <div class="card__content">
                                                                        <blockquote class="blockquote blockquote--card">
                                                                            
                                                                            <footer class="blockquote__footer">
                                                                                <cite class="blockquote__cite">
                                                                                    <span class="blockquote__author-name">Markus Keymore</span>
                                                                                    <span class="blockquote__author-info">Beleen Traveller</span>
                                                                                </cite>
                                                                            </footer>
                                                                        </blockquote>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!-- Player Card # / End -->
                                                        </div>
                                                    </div>
                                        </div>
                                        
                                        <div class="posts__more row">
                                                <a href="#" class="btn btn-inverse btn-sm btn-outline btn-icon-right btn-condensed">View all Experiences</a>
                                            </div>
                                    </div>
                        
                                </div>
                            </div>
                        </div>

    
            <div class="container">
        
                <div class="row">
                    <!-- Content -->

                    <div class="content col-md-12">

                            <!-- Post Author -->
                            <div class="post-author mt-5 mb-5 text-center">
                                <div class="post-author__info">
                                    <h2 class="">From Our Blog</h2>
                                    <span class="">News and tips from our travel journal...</span>
                                </div>
                            </div>
                            <!-- Post Author / End -->
        
                        <!-- Main News Banner -->
                        <div class="main-news-banner main-news-banner--soccer-ball" style="background-image: url(assets/images/why.png);">
                            <div class="main-news-banner__inner">
                                <div class="posts posts--simple-list posts--simple-list--xlg">
                                    <div class="posts__item posts__item--category-1">
                                        <div class="float-left col-md-4">
                                            
                                            <h2 class="text-white">Why should I book my holiday with Beleen Tours?</h2>

                                            <div class="posts__more">
                                                <a href="#" class="btn btn-primary btn-sm btn-icon-right">Read More <i class="fa fa-plus"></i></a>
                                            </div>
                                        </div>

                                        <ul class="col-md-4 float-right linkrs">
                                            <li class="">
                                                <a href="#" class="">Why should I book my holiday with Beleen Tours?</a>
                                            </li>
                                            <li class="">
                                                <a href="#" class="">When is the best time to visit 
                                                    Masai Mara?</a>
                                            </li>
                                            <li class="">
                                                <a href="#" class="">Things to do in Nairobi on 
                                                    your holiday.</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
        
                        </div>
                        <div class="posts__more row">
                            <a href="#" class="btn btn-inverse btn-sm btn-outline btn-icon-right btn-condensed">View all Articles</a>
                        </div>
                        <!-- Main News Banner / End -->

                        <div class="row bg-white p-5 iconboxrr">
                                <div class="col-md-4">
                                    <!-- Icobox -->
                                    <div class="icobox ">
                                        <div class="icobox__icon icobox__icon--border icobox__icon--lg icobox__icon--circle">
                                            <i class="icon-speech"></i>
                                        </div>
                                        <div class="icobox__content">
                                            <h4 class="icobox__title icobox__title--lg">Plan with an Expert</h4>
                                            <div class="icobox__description">
                                                Have a look at our original holiday 
experiences and then contact us with 
your brief, or call +254 725 450251
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Icobox / End -->
                                </div>
                                <div class="col-md-4">
                                    <!-- Icobox -->
                                    <div class="icobox ">
                                        <div class="icobox__icon icobox__icon--border icobox__icon--lg icobox__icon--circle">
                                            <i class="icon-compass"></i>
                                        </div>
                                        <div class="icobox__content">
                                            <h4 class="icobox__title icobox__title--lg">Expert Safari Guides</h4>
                                            <div class="icobox__description">
                                                Our experts guides will help plan your day with you, which can be as active or as leisurely as you wish.
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Icobox / End -->
                                </div>
                                <div class="col-md-4">
                                    <!-- Icobox -->
                                    <div class="icobox ">
                                        <div class="icobox__icon icobox__icon--border icobox__icon--lg icobox__icon--circle">
                                            <i class="fa fa-car"></i>
                                        </div>
                                        <div class="icobox__content">
                                            <h4 class="icobox__title icobox__title--lg">4x4 Equiped Safari Minivans</h4>
                                            <div class="icobox__description">
                                                Our specially designed safari minivans are available upon request for exclusive use to do unlimited game drives.
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Icobox / End -->
                                </div>
                            </div>
        
                    </div>
                    <!-- Content / End -->
        
                </div>
        
            </div>
        </div>
        
        <!-- Content / End -->

<?php get_footer(); ?>



