<?php
/**
 * Template Name: About Page Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package advoc
 */


get_header(); ?>

        
        <!-- ABOUT ME 1-->
        <!--  <section class="partner js-parallax-scroll p-t-75 p-b-75" style="background: url('images/bg-city-02.jpg') center center / cover no-repeat fixed;"> -->
        <section class="js-parallax-scroll about-me-1 service2 p-t-70 p-b-70" style="background: url('<?php echo esc_url( get_template_directory_uri() ); ?>/images/aboutmeme.jpg') right / contain no-repeat fixed;">
            <div class="container">
                <div class="row no-gutters">

                    <div class="col-lg-7 offset-lg-1 offset-lg--padding-70 pull-left">
                        <div class="about-me-1__content-wrap">
                            <div class="about-me-1__content">
                                <h5>i am Stephen Ouma</h5>
                                <h2>your perfect photographer</h2>
                                <p class="m-t-15 m-b-15">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dict sunt explicabo. Nemo
                                    enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed alt quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat.</p>
                                <div class="service-box">
                                    <div class="service-box__item">
                                        <div class="icon">
                                            <i class="fas fa-camera"></i>
                                        </div>
                                        <div class="text">
                                            <h4>
                                                <a href="service.html">professional</a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="service-box__item">
                                        <div class="icon">
                                            <i class="fas fa-cubes"></i>
                                        </div>
                                        <div class="text">
                                            <h4>
                                                <a href="service.html">unique</a>
                                            </h4>
                                        </div>
                                    </div>
                                    <div class="service-box__item">
                                        <div class="icon">
                                            <i class="fas fa-heartbeat"></i>
                                        </div>
                                        <div class="text">
                                            <h4>
                                                <a href="service.html">enthusiastic</a>
                                            </h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-lg-4--wider pull-right">
                        <!-- <div class="about-me-1__image">
                            <img src="images/aboutmeme.jpg" alt="Victor Umber">
                        </div> -->
                    </div>
                </div>
            </div>
        </section>
        <!-- END ABOUT ME 1-->





<?php get_footer(); ?>

