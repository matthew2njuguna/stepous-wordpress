<?php
/**
 * The template for displaying search results pages.
 *
 * @package ous.
 */
 
get_header(); ?>

        <!-- PAGE TITLE-->
        <section class="page-title js-parallax-scroll p-t-175 p-b-175" style="background-image: url(<?php echo esc_url( get_template_directory_uri() ); ?>/images/homebanner.png); background-repeat:no-repeat;">
            
            <div class="page-title__inner col-lg-6 text-left text-white p-l-100">
                <?php the_breadcrumb(); ?>
                <span class="label posts__cat-label">Search results</span>
                <h2 class="text-left"><?php global $wp_query; echo $wp_query->found_posts.' Search Results Found.'; ?></h2>
            </div>
        </section>
        <!-- END PAGE TITLE-->


        <!-- BLOG 4-->
        <section class="blog-4 p-t-50 p-b-100 js-list-load">
            <div class="container">
                <div class="row">
        
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>


                <div class="col-lg-4 col-md-6 ">
                        <div class="blog-item">
                            <div class="image">
                            <a href="<?php the_permalink(); ?>">
                            <?php if ( function_exists( 'add_theme_support' ) ) the_post_thumbnail('post-thumbnails'); ?>
                                </a>
                            </div>
                            <div class="content">
                                <h3 class="title">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </h3>
                                <p class="date">June 16 2017</p>
                                <p class="text"><?php the_excerpt(); ?></p>
                            </div>
                        </div>
                    </div>

                    <?php endwhile; else: ?>
                <div class="row">

                    <!-- Post Author -->
                    <div class="post-author mb-5">
                        <div class="post-author__info">
                            <h6 class=""><?php _e('Oops there was no result for your search query please try to search again'); ?></h6>
                        </div>
                        <div class="search-page-form p-t-50 text-white col-sm-12" id="ss-search-page-form"><?php get_search_form(); ?></div>
                    </div>
                </div>
            <?php endif; ?>

                </div>
            </div>
        </section>
        <!-- END BLOG 4  --> 


    
    <!-- Content / End -->
<?php //get_sidebar(); ?>
<?php get_footer(); ?>