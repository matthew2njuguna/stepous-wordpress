<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package Ous
 */

get_header(); ?>



<!-- <div class="pagering storopage maxwidthy">
<?php echo paginate_links( $args ); ?>

                                <div class="page-title-captions">
                                    <h1 class="entry-title"><?php the_category(); ?></h1>
                                    <div class="page-title-breadcrumbs">
                                        <div class="zozo-breadcrumbs"><?php the_breadcrumb(); ?></div>
                                    </div>
                                </div>
</div> -->

        <!-- PAGE TITLE-->
 

        <div class="container recent-blog subtr-brdcrbs">
            <div class="recent-blog__title-wrap clearfix m-b-90">
                <h2 class="recent-blog__title float-right wow fadeInUp" data-wow-delay=".2s"><?php the_title(); ?></h2>
            </div>
        </div>
        <!-- END PAGE TITLE-->
        <!-- ALBUM-->
        <section class="album-1 p-t-100 p-b-100 js-list-load">
            <div class="container">
                <div class="row">
                    <?php while (have_posts()) : the_post(); ?>
                    <div class="col-lg-4 col-md-6">
                        <div class="album-item">
                            <div class="album-item__image">
                                <a href="<?php the_permalink(); ?>">
                                <?php if ( function_exists( 'add_theme_support' ) ) the_post_thumbnail('team_thumb'); ?>
                                </a>
                            </div>
                            <h4 class="album-item__title  wow fadeInUp" data-wow-delay=".2s">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </h4>
                        </div>
                    </div>
                    <?php endwhile; // end of the loop. ?>
        
                    <!-- <div class="col-lg-4 col-md-6 js-load-item">
                        <div class="album-item">
                            <div class="album-item__image">
                                <a href="foliodet.html">
                                    <img src="images/client-say-01.jpg" alt="miracle in hand" />
                                </a>
                            </div>
                            <h4 class="album-item__title">
                                <a href="foliodet.html">miracle in hand</a>
                            </h4>
                        </div>
                    </div> -->
                   
                </div>
                <div class="see-more">
                    <a class="au-btn au-btn--dark au-btn--p50 js-load-btn" href="#">see more</a>
                </div>
            </div>
        </section>
        <!-- END ALBUM-->





    <!-- Jquery JS-->
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/bootstrap4/popper.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/bootstrap4/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/slick/slick.min.js">
    </script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/wow/wow.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/animsition/animsition.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/animejs/anime.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/isotope/isotope.pkgd.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/perfect-scrollbar/perfect-scrollbar.js">
    </script>

    <!-- Main JS-->
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/main.js"></script>
    <script type="text/javascript">



    </script>

<?php get_footer(); ?>