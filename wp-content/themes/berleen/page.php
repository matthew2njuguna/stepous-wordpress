<?php
/**
 * Template Name: Support Page Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package intrudex
 */


get_header(); ?>

  <!-- contact
   ================================================== -->
   <section id="contact" class="contact scroll"> 
  
      <?php while ( have_posts() ) : the_post(); ?>
      <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'singel_banner' ); ?>  
        <!-- PAGE TITLE-->
        <section class="page-title js-parallax-scroll p-t-175 p-b-175" style="background: url('<?php echo $backgroundImg[0]?>') center center / cover no-repeat fixed;">
        <div class="bg-overlay bg-overlay--opa30"></div>
            <div class="page-title__inner col-lg-6 text-left text-white p-l-100">
                <?php the_breadcrumb(); ?>
                <h2 class="text-left"><?php the_title(); ?></h2>
                <p class="p-t-30"><?php the_field('intro'); ?></p>
            </div>
        </section>
        <!-- END PAGE TITLE-->


        <!-- ABOUT US-->
        <section class="about-us-1 bg-f8 p-t-70 p-b-70">
            <div class="container">
                <div class="about-us-wrap">
                    <div class="row no-gutters">
                        <div class="col-lg-6">
                            <img src="<?php the_field('pastor_portrait'); ?>" alt="Pastor">
                        </div>
                        <div class="col-lg-6">
                            <div class="about-us-content">
                                <h5 class="title--small title--letter-space">A few words from </h5>
                                <h1 class="title--big">Our Pastor</h1>
                                <p class="m-b-15"><?php the_content(); ?></p>
                                <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                                <div class="service-box">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="service-box__item wow zoomIn">
                                                <div class="icon">
                                                    <i class="fas fa-shekel-sign"></i>
                                                </div>
                                                <div class="text">
                                                    <h4>
                                                        <a href="service.html">Fellowship</a>
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="service-box__item wow zoomIn" data-wow-delay=".2s">
                                                <div class="icon">
                                                    <i class="fab fa-rebel"></i>
                                                </div>
                                                <div class="text">
                                                    <h4>
                                                        <a href="service.html">Prayers</a>
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="service-box__item wow zoomIn" data-wow-delay=".4s">
                                                <div class="icon">
                                                    <i class="fas fa-map"></i>
                                                </div>
                                                <div class="text">
                                                    <h4>
                                                        <a href="service.html">Worship</a>
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="service-box__item wow zoomIn" data-wow-delay=".6s">
                                                <div class="icon">
                                                    <i class="fab fa-gg"></i>
                                                </div>
                                                <div class="text">
                                                    <h4>
                                                        <a href="service.html">Family</a>
                                                    </h4>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END ABOUT US-->
    <!-- CONTACT 2-->
    <section class="contact-2 p-t-70 p-b-60">
            <div class="container">
                <div class="contact-info">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-4 col-md-6">
                                <p>
                                    <i class="fas fa-home"></i>Ruiru Kamiti Rd Oppsite Kirigiti Stadium</p>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <p>
                                    <i class="fas fa-phone"></i>(+24) 1344 205 6999</p>
                            </div>
                            <div class="col-lg-4 col-md-6">
                                <p>
                                    <i class="fas fa-envelope"></i>pceakirigiti@gmail.com</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="map-wrapper js-google-map" data-makericon="images/icon/marker.png" data-makers="[[&quot;Umber&quot;, &quot;Now that you visited our website,&lt;br&gt; how about checking out our office too?&quot;, 40.715681, -74.003427]]">
                    <div class="map__holder js-map-holder" id="map"><?php the_field('map'); ?></div>
                </div>
         
            </div>
        </section>
        <!-- END CONTACT 2-->


        <!-- OUR TEAM-->
        <section class="our-team p-t-60 p-b-80">
            <div class="container">
                <h2 class="title title--36 title--semibold text-center">our team</h2>
                <div class="slide-wrapper js-slick-wrapper" data-slick-xl="4" data-slick-lg="4" data-slick-md="3" data-slick-sm="2" data-slick-xs="1" data-slick-loop="true" data-slick-autoplay="true">
                    <div class="slick-arrows">
                        <i class="zmdi zmdi-arrow-left js-slick-arrow-left prev"></i>
                        <i class="zmdi zmdi-arrow-right js-slick-arrow-right next"></i>
                    </div>
                    <div class="slide__content js-slick-content">
                      
                    <?php
                    $args=array(
                      'cat' =>$team,
                      'post_type' => 'post',
                      'post_status' => 'publish',
                      'orderby' => 'date','order' => 'ASC',
                      'posts_per_page'   => 12
                      );
                    $my_query = null;
                    $my_query = new WP_Query($args);
                    if( $my_query->have_posts() ) {
                       while ($my_query->have_posts()) : $my_query->the_post(); 
                       $post_id = $post->ID;
                ?>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
            <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'team_thumb' );?>
                        <div class="our-team__item">
                            <div class="our-team-img">
                                <div class="bg-overlay bg-overlay--opa30"></div>
                                <img src="<?php echo $thumb['0'];?>" alt="Allen Ward">
                                <div class="our-team-social">
                                    <a href="#">
                                        <i class="zmdi zmdi-facebook"></i>
                                    </a>
                                    <a href="#">
                                        <i class="zmdi zmdi-instagram"></i>
                                    </a>
                                    <a href="#">
                                        <i class="zmdi zmdi-google"></i>
                                    </a>
                                </div>
                            </div>
                            <div class="our-team-content">
                                <h4 class="name"><?php the_title(); ?></h4>
                                <span class="job">
                                    <em><?php the_field('designation'); ?> </em>
                                </span>
                            </div>
                        </div>
                        
                        <?php endwhile; } wp_reset_query(); ?> 
                    </div>
                </div>
            </div>
        </section>
        <!-- END OUR TEAM-->

        <!-- PARTNER-->
        <section class="partner js-parallax-scroll p-t-75 p-b-75" style="background: url('images/bg-city-02.jpg') center center / cover no-repeat fixed;">
            <div class="bg-overlay bg-overlay--opa80"></div>
            <div class="container">
                <h2 class="title title--36 title--semibold text-center text-white">Other Campuses </h2>
                <div class="slide-wrapper js-slick-wrapper" data-slick-xl="5" data-slick-lg="4" data-slick-md="2" data-slick-sm="3" data-slick-xs="2" data-slick-autoplay="true">
                    <div class="slide__content js-slick-content">
                      
                    <?php
                    $args=array(
                      'cat' =>$team,
                      'post_type' => 'post',
                      'post_status' => 'publish',
                      'orderby' => 'date','order' => 'ASC',
                      'posts_per_page'   => 12
                      );
                    $my_query = null;
                    $my_query = new WP_Query($args);
                    if( $my_query->have_posts() ) {
                       while ($my_query->have_posts()) : $my_query->the_post(); 
                       $post_id = $post->ID;
                ?>

                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      
                    <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'team_thumb' );?>
                        <div class="slide-item partner__item">
                            <a href="#">
                                <img src="<?php echo $thumb['0'];?>" alt="Turnkey">
                            </a>
                        </div>
                    <?php endwhile; } wp_reset_query(); ?> 
                    </div>
                </div>
            </div>
        </section>
        <!-- END PARTNER-->

      <?php endwhile; // end of the loop. ?>
  </section>
<?php get_footer(); ?>
