<?php
/**
 * Template Name: Gallery Page Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package chrisc
 */

get_header(); ?>
        <div class="ht"></div>
        <div class="block-vertical maxwidthy block-vertical-bottom">
            <div class="introdu">
              <div class="block-inner shorter">
                <div class="title">
                  Goodies From
                </div>
                <!-- .title -->
                <h1>Our Media <br>Gallery</h1>

              </div>
              <!-- .block-inner -->
            </div>
            <!-- .container -->
          </div>
          <div class="cov">
                    <?php 

                    $images = get_field('gallerymain');

                    if( $images ): ?>

                        <ul class="grid effect-6" id="gridey">
                            <?php foreach( $images as $image ): ?>
                                <li>
                                <a class="fancybox" rel="gallery1" href="<?php echo $image['url']; ?>" title="<?php echo $image['caption']; ?>">
                                    <img src="<?php echo $image['sizes']['media_thumb']; ?>" alt="<?php echo $image['alt']; ?>" />
                                </a>   
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    <?php endif; ?>
          </div>





    <script src="<?php bloginfo( 'template_directory' ); ?>/js/masonry.pkgd.min.js"></script>
    <script src="<?php bloginfo( 'template_directory' ); ?>/js/imagesloaded.js"></script>
    <script src="<?php bloginfo( 'template_directory' ); ?>/js/classie.js"></script>
    <script src="<?php bloginfo( 'template_directory' ); ?>/js/AnimOnScroll.js"></script>
    <script src="<?php bloginfo( 'template_directory' ); ?>/js/modernizr.custom.js"></script>
    <script>
      new AnimOnScroll( document.getElementById( 'gridey' ), {
        minDuration : 0.4,
        maxDuration : 0.7,
        viewportFactor : 0.2
      } );
    </script>


<?php get_footer(); ?>
