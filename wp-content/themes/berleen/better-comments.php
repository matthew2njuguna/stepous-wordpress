<?php
function better_comments( $comment, $args, $depth ) {
	global $post;
	$author_id = $post->post_author;
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case 'pingback' :
		case 'trackback' :
		// Display trackbacks differently than normal comments. ?>
	<li id="comment-<?php comment_ID(); ?>" <?php comment_class(); ?>>
		<div class="pingback-entry"><span class="pingback-heading"><?php esc_html_e( 'Pingback:', 'twenties' ); ?></span> <?php comment_author_link(); ?></div>
	<?php
		break;
		default :
		// Proceed with normal comments. ?>
	<li id="li-comment-<?php comment_ID(); ?>" class="parentlist pull-left">
		<article id="comment-<?php comment_ID(); ?>" <?php comment_class('pull-left m-b-50'); ?>>
			<div class="comment-author vcard pull-left">
				<?php echo get_avatar( $comment, 45 ); ?>
			</div><!-- .comment-author -->
			<div class="comment-details col-md-10 col-sm-12 pull-left">
				<header class="comment-meta">
					<b class="fn col-12 p-0 pull-left"><?php comment_author_link(); ?></b>
					<span class="comment-date pull-left">
					<?php printf( '<a href="%1$s"><time datetime="%2$s">%3$s</time></a>',
						esc_url( get_comment_link( $comment->comment_ID ) ),
						get_comment_time( 'c' ),
						sprintf( _x( '%1$s', '1: date', 'twenties' ), get_comment_date() )
					); ?> <?php esc_html_e( 'at', 'twenties' ); ?> <?php comment_time(); ?>
					</span><!-- .comment-date -->
					
					<div class="reply comment-reply-link pull-right">
						<?php comment_reply_link( array_merge( $args, array(
							'reply_text' => esc_html__( 'Reply', 'twenties' ),
							'depth'      => $depth,
							'max_depth'	 => $args['max_depth'] )
						) ); ?>
					</div><!-- .reply -->
				</header><!-- .comment-meta -->
				
				<?php if ( '0' == $comment->comment_approved ) : ?>
					<p class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'twenties' ); ?></p>
				<?php endif; ?>
				<div class="comment-content pull-left col-md-12 p-l-0 p-t-10">
					<?php comment_text(); ?>
				</div><!-- .comment-content -->
			</div><!-- .comment-details -->
		</article><!-- #comment-## -->
	<?php
		break;
	endswitch; // End comment_type check.
}