<?php
/*
Template Name: Archives
*/
get_header(); ?>



          <!-- Hero Slider
    ================================================== -->
    <div class="hero-slider-wrapper">
    
      <div class="hero-slider">
    
        <!-- Slide #0 -->
        <div class="hero-slider__item hero-slider__item--img1" style="background-image: url(<?php echo esc_url( get_template_directory_uri() ); ?>/assets/images/homebanner.jpeg);">
    
          <div class="container hero-slider__item-container">
            <div class="row">
              <div class="col-md-8 offset-md-2">
                <!-- Post Meta - Top -->
                <div class="post__meta-block post__meta-block--top">
    
                  <!-- Post Category -->
                  <div class="post__category">
                    <span class="label posts__cat-label"><?php the_category(); ?></span>
                  </div>
                  <!-- Post Category / End -->
    
                  <!-- Post Title -->
                                    <h1 class="page-heading__title">Ideas To Spark Your Imagination</h1>
                                        
    
    
                </div>
                <!-- Post Meta - Top / End -->
              </div>
            </div>
          </div>
    
        </div>
        <!-- Slide #0 / End -->
      </div>
    
    
    </div>


<?php 
// the query
$wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>-1)); ?>
 
<?php if ( $wpb_all_query->have_posts() ) : ?>


    <!-- Content
    ================================================== -->
    <div class="site-content">

    
                <div class="mb-5 gridr">
                    
                <div class="bg-white row p-3">
                    <div class="container">
                        <?php the_breadcrumb(); ?>
                       
                    </div>
                </div>
                    <div class="container">
                        <div class="row col-md-12"> 
                            <div class="post__meta-block post__meta-block--top col-md-12 p-0">
                                <!-- Post Author -->
                                <div class="post-author mb-5 mt-5">
                                    <div class="post-author__info">
                                        <h2 class="">Endless Experiences</h2>
                                    </div>
                                </div>
                                <!-- Post Author / End -->
                            </div>
       
 
    <!-- the loop -->
    <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>



    <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'team_thumb' );?>
                              <div class="col-md-4">
                            <!-- Player Card # -->
                            <div class="post-grid__item col-sm-12 team-roster__item ">
                                <div class="posts__item posts__item--card posts__item--category-1 card">
                                    <figure class="posts__thumb">
                                        <?php if(get_field('trip_stop')): ?>
                                        <div class="posts__cat">


                                                <?php while(has_sub_field('countries')): ?>

                                            <span class="label posts__cat-label"><?php the_sub_field('country'); ?></span>

                                                <?php endwhile; ?>



                                        </div>
                                        <?php endif; ?>
                                        <a href="#"><?php if ( function_exists( 'add_theme_support' ) ) the_post_thumbnail('pack_thumb'); ?></a>
                                        <span class="angled"></span>
                                    </figure>
                                    <div class="posts__inner card__content">
                                            <a href="<?php the_permalink(); ?>" class="posts__cta"><span class="expan hids">View details</span></a>
                                        <h6 class="posts__title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h6>
                                
                                        <time datetime="2016-08-23" class="posts__date"> <?php the_field('duration'); ?> Days</time>
        
                                        <?php if(get_field('trip_stop')): ?>

                                            <ul class="breadcrumb">

                                                <?php while(has_sub_field('trip_stop')): ?>

                                                    <li class="breadcrumb-item"><?php the_sub_field('trip_stop_chilld'); ?></li>

                                                <?php endwhile; ?>

                                            </ul>

                                        <?php endif; ?>
                                    </div>
                                    <footer class="posts__footer card__footer">
                                        <div class="post-author">
                                            <div class="post-author__info">
                                                   <!--  <a href="#" class="posts__nq btn btn-light hids">Please enquire</a> -->
                                                <h4 class="post-author__name">

                                            <div class="player-info-details__value"></div>
                                                <?php the_field('currency'); ?> <?php the_field('amount'); ?> <span class="text-gray"><?php the_field('ppav'); ?> </span></h4>
                                            </div>
                                        </div>
                                    </footer>
                                </div>
                            </div>
                            <!-- Player Card # / End -->
                        </div>   
    <?php endwhile; ?>
    <!-- end of the loop -->
                        </div>  
                    </div>
                </div>
 
    <?php wp_reset_postdata(); ?>
 
<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>
</div>
    
    <!-- Content / End -->	
<?php get_footer(); ?>