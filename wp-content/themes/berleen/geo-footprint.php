<?php
/**
 * Template Name: Geo Footprint Page Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package chrisc
 */

get_header(); ?>


     <!-- home
   ================================================== -->
   <section id="home" class="map">
      <div class="gradient-overlay"></div>
      <div id="map_wrapper">
        <div class="intro">
          <h1 class="animate-this pagetitle">Geo Footprint</h1>
          </div> 
          <div id="map"></div>
      </div>
  </section>


      

<?php get_footer(); ?>

<script type="text/javascript">
var locations = [
    [
        "Kenya",
 -1.153487, 36.210938
    ],
    [
        "Tanzania",
-6.950965, 34.628906
    ],
    [
        "Egypt",
26.498844, 29.487305,
    ],
    [
        "South Africa",
-25.825235, 28.103027,
    ],
    [
        "Mozambique",
-17.364074, 35.354004,
    ],    
    [
        "Nigeria",
6.418343, 3.405762,
    ],  
    [
        "Adis Ababa",
9.507984, 39.001465,
    ],  
    [
        "Rwanda",
-2.158989, 29.707031,
    ],
    [
        "Uganda",
0.213546, 32.607422,
    ]
]


    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 3,
      // center: new google.maps.LatLng(-33.92, 151.25),
      center: new google.maps.LatLng(-0.012360,35.507813),
    scrollwheel: false,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) {  
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0], locations[i][6]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }


</script>
