<?php
/**
 * The template for displaying single portfolio.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package mytheme
 */

get_header('home'); ?>



      <!-- HOME THUMB-->

      <div class="rev_slider_wrapper fullwidthbanner-container home-thumb">
              <a class="au-btn au-btn--border au-btn--p40 au-btn--border-6 position-absolute kickback" href="<?php echo site_url(); ?>/category/portfolio">Go Back</a>
        <div class="rev_slider fullwidthabanner js-rev" data-version="5.4.5" style="display:none;" data-rev-layout="fullscreen" data-rev-bullets="false" data-rev-thumbnails="true">
        <?php 

        $images = get_field('portfolio');

        if( $images ): ?>

          <ul>
              <?php foreach( $images as $image ): ?>
                <li class="rev-item" data-transition="parallaxtobottom" data-thumb="<?php echo $image['sizes']['upcoming_thumb']; ?>">
                    <img class="rev-slidebg" src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>">
                </li>
              <?php endforeach; ?>
            </ul>
            
        <?php endif; ?>
        </div>
      </div>
      <!-- END HOME THUMB-->


						
    <!-- Jquery JS-->
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/bootstrap4/popper.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/bootstrap4/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/slick/slick.min.js">
    </script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/wow/wow.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/animsition/animsition.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/animejs/anime.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/isotope/isotope.pkgd.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/perfect-scrollbar/perfect-scrollbar.js">
    </script>


    
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>//vendor/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/revolution/js/extensions/revolution.extension.video.min.js"></script>


    <!-- Main JS-->
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/main.js"></script>
    <script>
function myFunction() {
     document.getElementById("searchinput").focus();
}


$(".risto").show().delay(5000).queue(function(n) {
          $(this).addClass('showtrigd'); 
          n();
        });

        $(".risto.showtrigd").on('hover', function (e) {
            alert();
          $(this).removeClass('showtrigd');
        });
</script>



