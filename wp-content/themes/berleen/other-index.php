<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
get_header(); ?>






				<!-- Banner -->
					<section id="banner" class="major bxslider">
<?php 
// the query
$the_query = new WP_Query( array( 'cat' => 1 ) ); ?>

						
<?php if ( $the_query->have_posts() ) : ?>

	<!-- pagination here -->

						<li class="inner">
	<!-- the loop -->
	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
		<?php the_post_thumbnail('featured_banner'); ?>
		<header class="major">
			<h1><?php the_title(); ?></h1>
		</header>
		<div class="content">
			<p><?php the_excerpt(50); ?></p>
			<ul class="actions">
				<li><a href="<?php the_permalink(); ?>" class="button next scrolly">Get Started</a></li>
			</ul>
		</div>
	<?php endwhile; ?>
	<!-- end of the loop -->

						</li>
	<!-- pagination here -->

	<?php wp_reset_postdata(); ?>

<?php else : ?>
	<h1 class="cntalgn"><?php _e( 'Sorry, no posts matched your criteria.' ); ?></h1>
<?php endif; ?>


						
					</section>

				<!-- Main -->
					<div id="main">




<?php 
// the query
$the_query = new WP_Query( array( 'cat' => 2 ) ); ?>

						
<?php if ( $the_query->have_posts() ) : ?>

	<!-- pagination here -->
	<!-- the loop -->
	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>

						<!-- One -->
				<a href="<?php the_permalink(); ?>">
					<section id="one" class="tiles">
						<article>
							<span class="image">
								<?php if ( has_post_thumbnail() ) {the_post_thumbnail();} ?>
							</span>
							<header class="major">
								<h3><?php the_title(); ?></h3>
								<p><?php the_excerpt(); ?></p>
							</header>
						</article>
					<section>
				</a>
	
	<?php endwhile; ?>
	<!-- end of the loop -->

						</div>
	<!-- pagination here -->

	<?php wp_reset_postdata(); ?>

<?php else : ?>
	<h1 class="cntalgn"><?php _e( 'Sorry, no posts matched your criteria.' ); ?></h1>
<?php endif; ?>
					








						<!-- Two -->
							<section id="two">

<?php 
// the query
$the_query = new WP_Query( array( 'cat' => 3 ) ); ?>

						
<?php if ( $the_query->have_posts() ) : ?>

	<!-- pagination here -->
									<header class="major">
										<h2>Massa libero</h2>
									</header>
									<p>Nullam et orci eu lorem consequat tincidunt vivamus et sagittis libero. Mauris aliquet magna magna sed nunc rhoncus pharetra. Pellentesque condimentum sem. In efficitur ligula tate urna. Maecenas laoreet massa vel lacinia pellentesque lorem ipsum dolor. Nullam et orci eu lorem consequat tincidunt. Vivamus et sagittis libero. Mauris aliquet magna magna sed nunc rhoncus amet pharetra et feugiat tempus.</p>
								
	<!-- the loop -->
	<?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
						<!-- One -->
				<a href="<?php the_permalink(); ?>">
					<div class="inner">
						<header class="major">
							<h2><?php the_title(); ?></h2>
						</header>
						<p><?php the_excerpt(); ?></p>	
						<ul class="actions">
							<li><a href="<?php the_permalink(); ?>" class="button next">Get Started</a></li>
						</ul>
					</div>
				</a>

	<?php endwhile; ?>
	<!-- end of the loop -->
	<!-- pagination here -->

	<?php wp_reset_postdata(); ?>

<?php else : ?>
	<h1 class="cntalgn"><?php _e( 'Sorry, no posts matched your criteria.' ); ?></h1>
<?php endif; ?>
					

							</section>

					</div>

				<!-- Contact -->
					<section id="contact">
						<div class="inner">
							<section>
								<form method="post" action="#">
									<div class="field half first">
										<label for="name">Name</label>
										<input type="text" name="name" id="name" />
									</div>
									<div class="field half">
										<label for="email">Email</label>
										<input type="text" name="email" id="email" />
									</div>
									<div class="field">
										<label for="message">Message</label>
										<textarea name="message" id="message" rows="6"></textarea>
									</div>
									<ul class="actions">
										<li><input type="submit" value="Send Message" class="special" /></li>
										<li><input type="reset" value="Clear" /></li>
									</ul>
								</form>
							</section>
							<section class="split">
								<section>
									<div class="contact-method">
										<span class="icon alt fa-envelope"></span>
										<h3>Email</h3>
										<a href="#">information@untitled.tld</a>
									</div>
								</section>
								<section>
									<div class="contact-method">
										<span class="icon alt fa-phone"></span>
										<h3>Phone</h3>
										<span>(000) 000-0000 x12387</span>
									</div>
								</section>
								<section>
									<div class="contact-method">
										<span class="icon alt fa-home"></span>
										<h3>Address</h3>
										<span>1234 Somewhere Road #5432<br />
										Nashville, TN 00000<br />
										United States of America</span>
									</div>
								</section>
							</section>
						</div>
					</section>

<?php
// get_sidebar();
get_footer();
