<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package chrisc
 */

get_header(); ?>
<?php 
$slideCatIn = 7;
?>

        <!-- SILDER-->
        
        <div class="container recent-blog subtr-brdcrbs">
            <div class="recent-blog__title-wrap clearfix m-b-90">
                <h2 class="recent-blog__title float-right wow fadeInUp" data-wow-delay=".2s"><?php the_category(', ') ?></h2>
            </div>
        </div>
        <!-- AUTUMN-->
        <div class="container p-t-100">

            <div class="card-columns">
      
            <?php while (have_posts()) : the_post(); ?>
            <div class="card pull-left">
                <a class="" href="<?php the_permalink() ?>">
                    <?php if ( function_exists( 'add_theme_support' ) ) the_post_thumbnail('cause_thumb'); ?>
                    <div class="card-body">
                        <h5 class="card-title wow fadeInUp" data-wow-delay=".2s"><?php the_title() ?></h5>
                        <p class="card-text wow fadeInUp" data-wow-delay=".2s"><?php the_excerpt() ?></p>
                        <p class="card-text wow fadeInUp" data-wow-delay=".2s"><small class="text-muted"><?php $post_date = get_the_date( 'l F j, Y' ); echo $post_date; ?></small></p>
                    </div>
                </a>
            </div>
                              <?php endwhile; // end of the loop. ?>


                                     
            <?php echo paginate_links( $args ); ?>      
        </div>
        <!-- END SUMMER -->

<?php get_footer(); ?>