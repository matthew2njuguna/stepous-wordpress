
<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="au theme template">
    <meta name="author" content="Hau Nguyen">
    <meta name="keywords" content="au theme template">

    <!-- Title Page-->
    <title>Stephen Ouma Photo</title>

    <!-- Fontfaces CSS-->
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/font-face.css" rel="stylesheet" media="all">
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/bootstrap4/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- Vendor CSS-->
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link rel="icon" href="<?php echo esc_url( get_template_directory_uri() ); ?>/images/icon/stevo.jpg">
    <link href="<?php echo esc_url( get_template_directory_uri() ); ?>/css/theme.css" rel="stylesheet" media="all">

</head>

<body class="animsition black" >
    <div class="page-wrapper">
        <!-- HEADER DESKTOP-->
                <!-- HEADER DESKTOP-->
                <header class="header-desktop header-1 d-none d-lg-block loginbg">
            <div class="container">
                <div class="section__content section__content--w1760">
                    <div class=" header__content">
                        <div class="col-md-12">

                            <div class="col-md-2 mr-auto">
                                <a href="<?php echo site_url(); ?>" class="mt-3">
                                <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/icon/logo-white.png" alt="PCEA Kirigiti" />
                                </a>
                            </div>

                            <nav class="nav-header col-md-10">
                            
                            <?php if ( has_nav_menu( 'primary' ) ) : ?>
                                    
                                    <?php
                                        wp_nav_menu( array(
                                            'theme_location' => 'primary',
                                            'menu_class'     => 'list-unstyled pull-right list-inline',
                                          
                                        ) );
                                    ?>                
                        
                                <?php endif; ?>
                            </nav>

                        </div>
                    </div>
                </div>
            </div>
        </header>


        <!-- HEADER MOBILE-->
        <header class="header-mobile d-block d-lg-none">
            <div class="header-mobile__bar">
                <div class="container-fluid clearfix">
                    <a class="logo" href="<?php echo site_url(); ?>">
                    <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo-black.png" alt="" />
                            </a>
                    <button class="hamburger hamburger--slider float-right" type="button">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
            <nav class="navbar-mobile">
                <div class="container-fluid">
                          
                <?php if ( has_nav_menu( 'primary' ) ) : ?>
                                    
                                    <?php
                                        wp_nav_menu( array(
                                            'theme_location' => 'primary',
                                            'menu_class'     => 'navbar-mobile__list list-unstyled',
                                          
                                        ) );
                                    ?>                
                        
                                <?php endif; ?>
                </div>
            </nav>
        </header>
        <!-- END HEADER MOBILE-->

