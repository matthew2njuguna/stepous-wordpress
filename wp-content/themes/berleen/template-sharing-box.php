<?php 
/* Social Share Buttons template for Wordpress
 * By Daan van den Bergh 
 */ 

$postUrl = 'http' . ( isset( $_SERVER['HTTPS'] ) ? 's' : '' ) . '://' . "{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}"; ?>

<section class="sharing-box content-margin content-background clearfix">
    <h2 class="sharing-box-name text-white p-b-10">Don't be selfish.<br> Share the knowledge!</h2>
    <div class="share-button-wrapper p-b-30">
        <a target="_blank" class="share-button share-twitter" href="https://twitter.com/intent/tweet?url=<?php echo $postUrl; ?>&text=<?php echo the_title(); ?>&via=<?php the_author_meta( 'twitter' ); ?>" title="Tweet this"> <span class="badge text-white"><i class="fab fa-twitter"></i></span></a>
        <a target="_blank" class="share-button share-facebook" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $postUrl; ?>" title="Share on Facebook"> <span class="badge text-white"><i class="fab fa-facebook-f"></i></span></a>
        <a target="_blank" class="share-button share-googleplus" href="https://plus.google.com/share?url=<?php echo $postUrl; ?>" title="Share on Google+"> <span class="badge text-white"><i class="fab fa-google-plus-g"></i></span></a>
    </div>
</section>
<!-- <button type="button" class="btn">
                                <span class="text-white"><span class="icon-eye"></span></span>
                                <span class="badge text-white">23</span>
                            </button>
                            <button type="button" class="btn">
                                <span class="badge text-white"><span class="icon-social-facebook-circular"></span></span>
                            </button>
                            <button type="button" class="btn">
                                <span class="badge text-white"><span class="icon-social-twitter-circular"></span></span>
                            </button>
                            <button type="button" class="btn">
                                <span class="badge text-white"><span class="icon-social-pinterest-circular"></span></span>
                            </button> -->