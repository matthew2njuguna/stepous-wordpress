<?php
// Make theme available for translation
// Translations can be filed in the /languages/ directory
load_theme_textdomain( 'beleen', TEMPLATEPATH . '/languages' );
 
$locale = get_locale();
$locale_file = TEMPLATEPATH . "/languages/$locale.php";
if ( is_readable($locale_file) )
    require_once($locale_file);
 

wp_enqueue_style('main-styles', get_template_directory_uri() . '/style.css', array(), filemtime(get_template_directory() . '/style.css'), false);

// Include better comments file
require_once( get_template_directory() .'/better-comments.php' );

//custom post types
//  add_action( 'init', 'create_post_type' );
// function create_post_type() {
//   register_post_type( 'news',
//     array(
//       'labels' => array(
//         'name' => __( 'News' ),
//         'singular_name' => __( 'News' )
//       ),
//       'public' => true,
//       'has_archive' => true,
//     )
//   );
// }


add_action('wp_enqueue_scripts', 'vb_print_scripts');

function vb_print_scripts() {
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-form');
    
    $ajaxurl = admin_url('admin-ajax.php');
    $ajax_nonce = wp_create_nonce('MailChimp');
    wp_localize_script( 'jquery-core', 'ajaxVars', array( 'ajaxurl' => $ajaxurl, 'ajax_nonce' => $ajax_nonce ) );
}


add_action('wp_ajax_nopriv_add_to_mailchimp_list', 'add_to_mailchimp_list');

function add_to_mailchimp_list() {

    check_ajax_referer('MailChimp', 'ajax_nonce');
    $_POST = array_map('stripslashes_deep', $_POST);

    $email = sanitize_email($_POST['email']);

    if (is_email($email)) {
        $submit_url = 'http://us7.api.mailchimp.com/1.3/?method=listSubscribe';
        
        $data = array(
            'email_address' => $email,
            'apikey'    => '453ff149737556df84813bbeffad9ddf-us7',
            'id'        => 'daea2d6b22',
            'double_optin'  => true,
            'send_welcome'  => false,
            'email_type'    => 'html'
        );
        
        $payload = json_encode($data);
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $submit_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, urlencode($payload));
         
        $result = curl_exec($ch);
        curl_close ($ch);
        $data = json_decode($result);

        if ($data === true) {
            echo 'added';
        } else if (is_object($data) && $data->error) {
            echo ($data->code === 214) ? 'already subscribed' : 'error';        
        } else if (is_null($data)) {
            echo 'error';
        }
    } else {
        echo 'invalid email';   
    }
    die();
}


// Creates Movie post types Custom Post Type
// function movie_reviews_init() {
//     $args = array(
//       'label' => 'Staff',
//         'public' => true,
//         'show_ui' => true,
//         'capability_type' => 'post',
//         'hierarchical' => false,
//         'rewrite' => array('slug' => 'movie-reviews'),
//         'query_var' => true,
//         'menu_icon' => 'dashicons-heart',
//         'supports' => array(
//             'title',
//             'editor',
//             'excerpt',
//             'trackbacks',
//             'custom-fields',
//             'comments',
//             'revisions',
//             'thumbnail',
//             'author',
//             'page-attributes',)
//         );
//     register_post_type( 'movie-reviews', $args );
// }
// add_action( 'init', 'movie_reviews_init' );



// Get the page number
function get_page_number() {
    if ( get_query_var('paged') ) {
        print ' | ' . __( 'Page ' , 'pf') . get_query_var('paged');
    }
} // end get_page_number




function wpse_category_single_template( $single_template ) {
    global $post;
    $all_cats = get_the_category();

    if ( $all_cats[0]->cat_ID == '3' ) {
        if ( file_exists(get_template_directory() . "/single-portfolio.php") ) return get_template_directory() . "/single-portfolio.php";
    } elseif ( $all_cats[0]->cat_ID == '16' ) {
        if ( file_exists(get_template_directory() . "/single-catproject.php") ) return get_template_directory() . "/single-catproject.php";
    }
    return $single_template;
}
add_filter( 'single_template', 'wpse_category_single_template' );




// custom excerpt length

function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

// remove p tegs
remove_filter( 'the_content', 'wpautop' );


// Remove p tags from author description.
remove_filter ('acf_the_content', 'wpautop');

// This theme uses wp_nav_menu() in 2 location.
register_nav_menus( array(
	'primary' => 'Primary Navigation',
    'secondary' => 'Footer navigation'
) );

function add_classes_on_li($classes, $item, $args) {
    $classes[] = 'list-inline-item has-mega';
    return $classes;
}
add_filter('nav_menu_css_class','add_classes_on_li',1,3);

register_sidebar(array(
        'before_widget' => '',
        'after_widget' => '',
        'before_title' => '<div class="title">',
        'after_title' => '</div>',
    ));
	
//add_theme_support( 'post-formats', array( 'aside', 'gallery' ) );
add_theme_support( 'post-formats', array(
		'aside', 'audio', 'chat', 'gallery', 'image', 'link', 'quote', 'status', 'video'
	) );	
	
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(960, 638, true);
    add_image_size( 'upcoming_thumb', 640, 896, true);
    add_image_size( 'featured_banner', 1760, 761, true);
    add_image_size( 'sermons_thumb', 960, 585, true);
    add_image_size( 'events_thumbig', 640, 1076, true);
    add_image_size( 'event_thumbsmall', 640, 853, true);
    add_image_size( 'events_long', 960, 573, true);
    add_image_size( 'singel_banner', 1920, 920, true);
    add_image_size( 'mobile_thumb', 375, 812, true);

function home_page_menu_args( $args ) {
$args['show_home'] = true;
return $args;
}
add_filter( 'wp_page_menu_args', 'home_page_menu_args' );


function my_custom_login_logo() {
    echo '<style type="text/css">
        h1 a { background-image:url('.get_bloginfo('template_directory').'/images/icon/logo-black.png) !important; 
            height:100px;
            width:100px;
            background-size: cover;
            background-repeat: no-repeat;
            padding-bottom: 20px;
        }
        
    </style>';
}

function login_background() {
    echo '<style type="text/css">
    body { background: #ffffff; }
    .login form { background: #f2f2f2; }
    </style>';
    }
    add_action('login_head', 'login_background');


add_action('login_head', 'my_custom_login_logo');

add_filter('show_admin_bar', '__return_false');

function wpb_login_logo_url() {
    return home_url();
}
add_filter( 'login_headerurl', 'wpb_login_logo_url' );

function wpb_login_logo_url_title() {
    return 'Stephen Ouma';
}
add_filter( 'login_headertitle', 'wpb_login_logo_url_title' );




// Removes ul class from wp_nav_menu
function remove_ul ( $menu ){
    return preg_replace( array( '#^<ul[^>]*>#', '#</ul>$#' ), '', $menu );
}
add_filter( 'wp_nav_menu', 'remove_ul' );


function clean_value($str) {
    $str = trim($str);
    $str = preg_replace("@<script[^>]*>.+</script[^>]*>@i", "", $str);
    $str = preg_replace("@<style[^>]*>.+</style[^>]*>@i", "", $str);
    $str = strip_tags($str);
    //$str=mysql_real_escape_string($str);
    //return addslashes($str);
	return $str;
}


function the_breadcrumb() {
        echo '<ul id="crumbs" class="breadcrumb bready">';
    if (!is_home()) {
        echo '<li class="breadcrumb-item"><a href="';
        echo get_option('home');
        echo '">';
        echo 'Home';
        echo "</a></li>";
        if (is_category() || is_single()) {
            echo '<li class="breadcrumb-item">';
            the_category(' </li><li class="breadcrumb-item">');
            if (is_single()) {
                echo "</li>";
                the_title();
                echo '</li>';
            }
        } elseif (is_page()) {
            echo '<li class="breadcrumb-item">';
            echo the_title();
            echo '</li>';
        }
    }
    elseif (is_tag()) {single_tag_title();}
    elseif (is_day()) {echo"<li>Archive for "; the_time('F jS, Y'); echo'</li>';}
    elseif (is_month()) {echo"<li>Archive for "; the_time('F, Y'); echo'</li>';}
    elseif (is_year()) {echo"<li>Archive for "; the_time('Y'); echo'</li>';}
    elseif (is_author()) {echo"<li>Author Archive"; echo'</li>';}
    elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<li>Blog Archives"; echo'</li>';}
    elseif (is_search()) {echo"<li>Search Results"; echo'</li>';}
    echo '</ul>';
}



/**
 * Add HTML5 theme support.
 */
function wpdocs_after_setup_theme() {
    add_theme_support( 'html5', array( 'search-form' ) );
}
add_action( 'after_setup_theme', 'wpdocs_after_setup_theme' );

add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);

function special_nav_class ($classes, $item) {
    if (in_array('current-menu-item', $classes) ){
        $classes[] = 'active ';
    }
    return $classes;
}

// function to display number of posts.
function getPostViews($postID){
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}
 

function wporg_remove_category_title( $title ) {
    if ( is_category() ) {
        $title = single_cat_title( '', false );
    }
    return $title;
}
add_filter( 'remove_the_archive_title', 'wporg_remove_category_title' );

// function to count views.
function setPostViews($postID) {
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}