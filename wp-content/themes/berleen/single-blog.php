<?php
/*
 * Template Name: Single Blog
 * Template Post Type: post, page, product
 */
  
 get_header();  ?>



<?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>  

        <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'singel_banner' ); ?>  
        <!-- PAGE TITLE-->
        <section class="page-title js-parallax-scroll p-t-175 p-b-175" style="background: url('<?php echo $backgroundImg[0]?>') center center / cover no-repeat fixed;">
        <div class="bg-overlay bg-overlay--opa30"></div>
            <div class="page-title__inner col-lg-6 text-left text-white p-l-100">
                <?php the_breadcrumb(); ?>
                <h2 class="text-left"><?php the_title(); ?></h2>
                <p class="p-t-30"><?php the_field('intro'); ?></p>
            </div>
        </section>
        <!-- END PAGE TITLE-->

        <!-- PROJECT INFO 3-->
        <section class="project-info-1 project-info-3 p-t-50 p-b-70">
            <div class="container">
                
                <p class="p-t-30 p-b-50"><b><em><?php the_field('introbold'); ?></em></b></p>
                <div class="project-image-wrap">
                    <div class="row">
                        <div class="col-lg-7">
                            <div class="project-image">
                                        <img src="<?php the_field('contbold'); ?>" alt="">
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="project-image">
                                        <img src="<?php the_field('image1'); ?>" alt="">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="project-image">
                                        <img src="<?php the_field('image2'); ?>" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="project-image">
                                        <img src="<?php the_field('image3'); ?>" alt="">
                            </div>
                            <div class="project-image">
                                        <img src="<?php the_field('image4'); ?>" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="project-info__text p-t-100 col-lg-8 p-r-30"><?php the_content(); ?></div>
                    <div class="col-lg-4 p-t-100 p-l-30"><?php get_sidebar(); ?></div>
                </div>
                <div class="project-info-arrows">
                    <a class="prev" href="#">
                        <i class="zmdi zmdi-arrow-left"></i>prev Read</a>
                    <a class="next" href="#">next Read
                        <i class="zmdi zmdi-arrow-right"></i>
                    </a>
                </div>
            </div>
        </section>
        <!-- END PROJECT INFO 3-->

        
        <?php endwhile; else : ?>
                                                <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
                                            <?php endif; ?>




    
								
<?php get_footer(); ?>
