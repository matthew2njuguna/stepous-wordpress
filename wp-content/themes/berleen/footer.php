


<?php 
$upTo = 11;
?>

        <!-- FOOTER-->
        <footer class="footer footer--dark">
            <div class="container">
                <div class="row">
<!--                     <a class="" href="storiedet.html" tabindex="-1">
                        <div class="slide-item risto clearfix col-lg-4" style="background:url('images/client-say-01.jpg') center center / cover no-repeat;">
                            <div class="slide-text" data-animation="fadeInUp">
                                <h5 class="text-primary">What I've Been Upto</h5>
                                <h2 class="m-b-15 text-white">Dandora Dumpsite</h2>
                            </div>
                        </div>
                    </a> -->


                    
                    <?php
                            $args=array(
                            'cat' =>$upTo,
                            'post_type' => 'post',
                            'post_status' => 'publish',
                            'orderby' => 'date','order' => 'ASC',
                            'posts_per_page'   => 1
                            );
                            $my_query = null;
                            $my_query = new WP_Query($args);
                            if( $my_query->have_posts() ) {
                            while ($my_query->have_posts()) : $my_query->the_post(); 
                            $post_id = $post->ID;
                        ?>
                        
                            <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'featured_banner' );?>
                    <a class="col-lg-4" href="<?php the_permalink() ?>" tabindex="-1" style="background-image: url('<?php echo $thumb['0'];?>">
                        <h5 class="text-primary">What I've Been Upto</h5>
                        <h2 class="m-b-15 text-white"><?php the_title(); ?></h2>
                    </a>
                    
                            
                    <?php endwhile; } wp_reset_query(); ?> 
                    <div class="col-lg-4">
                        <div class="footer__social">
                            <a href="#">
                                <i class="zmdi zmdi-facebook"></i>
                            </a>
                            <a href="#">
                                <i class="zmdi zmdi-instagram"></i>
                            </a>
                            <a href="#">
                                <i class="zmdi zmdi-twitter"></i>
                            </a>
                            <a class="mr-0" href="#">
                                <i class="zmdi zmdi-dribbble"></i>
                            </a>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="footer__copyright">© 2018 Stephen Ouma</div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- END FOOTER -->
    </div>

    <!-- Jquery JS-->
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap JS-->
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/bootstrap4/popper.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/bootstrap4/bootstrap.min.js"></script>
    <!-- Vendor JS       -->
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/slick/slick.min.js">
    </script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/wow/wow.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/animsition/animsition.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/animejs/anime.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/isotope/isotope.pkgd.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/bootstrap-progressbar/bootstrap-progressbar.min.js">
    </script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/counter-up/jquery.waypoints.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/counter-up/jquery.counterup.min.js">
    </script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/circle-progress/circle-progress.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/perfect-scrollbar/perfect-scrollbar.js">
    </script>


    
<script src="<?php echo esc_url( get_template_directory_uri() ); ?>//vendor/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/vendor/revolution/js/extensions/revolution.extension.video.min.js"></script>


    <!-- Main JS-->
    <script src="<?php echo esc_url( get_template_directory_uri() ); ?>/js/main.js"></script>
    <script>
function myFunction() {
     document.getElementById("searchinput").focus();
}


$(".risto").show().delay(5000).queue(function(n) {
          $(this).addClass('showtrigd'); 
          n();
        });

        $(".risto.showtrigd").on('hover', function (e) {
            alert();
          $(this).removeClass('showtrigd');
        });
</script>
</body>

</html>
<!-- end document-->
<!-- end document-->

