<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package chrisc
 */

get_header(); ?>

         <!-- PAGE TITLE-->
         <section class="page-title js-parallax-scroll p-t-175 p-b-175" style="background-image: url(<?php echo esc_url( get_template_directory_uri() ); ?>/images/homebanner.png); background-repeat:no-repeat;">
            
            <div class="page-title__inner col-lg-6 text-left text-white p-l-100">
                <h2 class="text-left"><?php single_cat_title('Checkout Our '); ?></h2>
                <p class="p-t-30">Intentionally relevant. All non-denom. Lean into some totally relational placeholder text for your next website project. Churchly ipsum dolor amet […]</p>
            </div>
        </section>
        <!-- END PAGE TITLE-->

        <!-- BLOG 4-->
        <section class="blog-4 p-t-50 p-b-100 js-list-load">
            <div class="container">
                <div class="row">
        
                <?php while (have_posts()) : the_post(); ?>


                <div class="col-lg-4 col-md-6 ">
                        <div class="blog-item">
                            <div class="image">
                            <a href="<?php the_permalink(); ?>">
                            <?php if ( function_exists( 'add_theme_support' ) ) the_post_thumbnail('post-thumbnails'); ?>
                                </a>
                            </div>
                            <div class="content">
                                <h3 class="title">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                </h3>
                                <p class="date">June 16 2017</p>
                                <p class="text"><?php the_excerpt(); ?></p>
                            </div>
                        </div>
                    </div>

                    <?php endwhile; // end of the loop. ?>

                </div>
                <div class="load-more text-center">
                    <a class="au-btn au-btn--dark au-btn--p45 m-t-35 js-load-btn" href="#">load more</a>
                </div>
            </div>
        </section>
        <!-- END BLOG 4  -->





<?php get_footer(); ?>