<?php get_header(); ?>




<!-- The Loop -->

        <!-- AUTUMN-->
        <div class="container p-t-100">

    <?php
    $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
    ?>

        <div class="container recent-blog subtr-brdcrbs">
            <div class="recent-blog__title-wrap clearfix m-b-90">
                <h2 class="recent-blog__title float-right wow fadeInUp" data-wow-delay=".2s">Author: <?php echo $curauth->nickname; ?></h2>
            </div>
        </div>
    <dl>
        <dt>Website</dt>
        <dd><a href="<?php echo $curauth->user_url; ?>"><?php echo $curauth->user_url; ?></a></dd>
        <dt>About <?php echo $curauth->nickname; ?></dt>
        <dd><?php echo $curauth->user_description; ?></dd>
    </dl>

            <h2>Posts by <?php echo $curauth->nickname; ?>:</h2>
            <div class="card-columns">
      
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <div class="card pull-left">
                <a class="" href="<?php the_permalink() ?>">
                    <?php if ( function_exists( 'add_theme_support' ) ) the_post_thumbnail('cause_thumb'); ?>
                    <div class="card-body">
                        <h5 class="card-title wow fadeInUp" data-wow-delay=".2s"><?php the_title() ?></h5>
                        <p class="card-text wow fadeInUp" data-wow-delay=".2s"><?php the_excerpt() ?></p>
                        <p class="card-text wow fadeInUp" data-wow-delay=".2s"><small class="text-muted"><?php $post_date = get_the_date( 'l F j, Y' ); echo $post_date; ?></small></p>
                    </div>
                </a>
            </div>
            <?php endwhile; else: ?>
        <p><?php _e('Oops no posts by this author.'); ?></p>

    <?php endif; ?>


             <!-- <div class="cause-over-title"><span class="raised-money"><?php the_category(', ') ?></span>
            <br><span class="total-goal"><?php the_tags(); ?> </span><?php the_field('locale'); ?></div>
                                     
            <?php echo paginate_links( $args ); ?>
             <div class="card p-3 text-right">
                <a class="" href="storiedet.html">
                <blockquote class="blockquote mb-0">
                  <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer posuere erat a ante.</p>
                  <footer class="blockquote-footer">
                    <small class="text-muted">
                      Someone famous in <cite title="Source Title">Source Title</cite>
                    </small>
                  </footer>
                </blockquote>
                </a>
              </div> -->
            </div>            
        </div>
        <!-- END SUMMER -->


<!-- End Loop -->

    </ul>
</div>

<?php get_footer(); ?>