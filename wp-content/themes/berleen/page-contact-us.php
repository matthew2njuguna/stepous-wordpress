<?php
 
  //response generation function
 
  $response = "";
 
  //function to generate response
  function generate_response($type, $message){
   
    global $response;
 
    if($type == "success") $response = "<div class='success'>{$message}</div>";
    else $response = "<div class='error'>{$message}</div>";
   
  }
 
  //response messages
  $not_human       = "Human verification incorrect.";
  $missing_content = "Please supply all information.";
  $email_invalid   = "Email Address Invalid.";
  $message_unsent  = "Message was not sent. Try Again.";
  $message_sent    = "Thanks! Your message has been sent.";
 
  //user posted variables
  $name = $_POST['message_name'];
  $email = $_POST['message_email'];
  $subject = $_POST['message_subject'];
  $message = $_POST['message_text'];
  $human = $_POST['message_human'];
 
  //php mailer variables
  $to = get_option('admin_email', 'matthew2njuguna@gmail.com');
  $subject = "Someone sent a message from ".get_bloginfo('name');
  $headers = 'From: '. $email . "\r\n" .
    'Reply-To: ' . $email . "\r\n";
 
  if(!$human == 0){
    if($human != 2) generate_response("error", $not_human); //not human!
    else {
     
      //validate email
      if(!filter_var($email, FILTER_VALIDATE_EMAIL))
        generate_response("error", $email_invalid);
      else //email is valid
      {
        //validate presence of name and message
        if(empty($name) || empty($message)){
          generate_response("error", $missing_content);
        }
        else //ready to go!
        {
          $sent = mail($to, $subject, $message, $headers);
          if($sent) generate_response("success", $message_sent); //message sent!
          else generate_response("error", $message_unsent); //message wasn't sent
        }
      }
    }
  }
  else if ($_POST['submitted']) generate_response("error", $missing_content);
 
?>
 
<?php get_header(); ?>


      <?php while ( have_posts() ) : the_post(); ?>
            

  <!-- Page Heading
    ================================================== -->
    <div class="page-heading">
      <div class="container">
        <div class="row">
          <div class="col-md-10 offset-md-1">
            <h1 class="page-heading__title">Contact <span>Us</span></h1>
          </div>
        </div>
      </div>
    </div>
    <!-- Page Heading / End -->
    
    
    <!-- Content
    ================================================== -->
    <div class="site-content pb-0">

                <div class="bg-white row p-3">
                        <div class="container">
                            <ol class="breadcrumb bready">
                              <?php the_breadcrumb(); ?>
                            </ol>   
                        </div>
                    </div>
      <div class="container">
    
        <!-- Contact Area -->
        <div class="card bg-transparent">
          <div class="card__content p-0">
    
            <div class="row">
              <div class="col-md-5 bl-1 pt-5">
                <div class="widget__content card__content p-0">

                <ul class="commentary p-0">
            
   
            
                  <li class="commentary__item pl-0">
                    <div class="commentary__icon">
                      <i class="icon-call-in"></i>
                    </div>
                    <div class="commentary__icon">
                      
                      <h2 class="widget-game-result__title">Phone</h2>
                      <p>+254-751-142-410</p> 
                  </li>
            
                  <li class="commentary__item pl-0">
                    <div class="commentary__icon">
                      <i class="icon-envelope-letter"></i>
                    </div>
                    <div class="commentary__icon">
                      
                      <h2 class="widget-game-result__title">Email</h2>
                      <p>tours@berleensafaris.com</p> 
                  </li>


                </ul>
              </div>
                <address class="row">
                  <strong>Mondays to Fridays:</strong> 9:00am to 12:00pm
                </address>
              </div>
              <div id="respond" class="col-md-6 ml-auto pt-5" >
    
                
              <?php the_content(); ?> 
                <!-- Contact Form / End -->
              </div>
            </div>
    
          </div>
        </div>
        <!-- Contact Area / End -->
    
      </div>
    </div>
    
    <!-- Content / End -->


      <?php endwhile; // end of the loop. ?>
<?php get_footer(); ?>