<?php
/**
 * Template Name: Home Page Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package advoc
 */

get_header(); ?>


<?php 
$featured = 0;
?>

        <!-- SILDER-->
        <section class="slide slide-1">
            <div class="container-fluid">
                <div class="section__content section__content--w1760">
                    <div class="slide-wrapper js-slick-wrapper" data-slick-autoplay="true" data-slick-fade="true">
                        <div class="slide__content js-slick-content">
                        <?php
                            $args=array(
                            'cat' =>$featured,
                            'post_type' => 'post',
                            'post_status' => 'publish',
                            'orderby' => 'date','order' => 'ASC',
                            'posts_per_page'   => 12
                            );
                            $my_query = null;
                            $my_query = new WP_Query($args);
                            if( $my_query->have_posts() ) {
                            while ($my_query->have_posts()) : $my_query->the_post(); 
                            $post_id = $post->ID;
                        ?>
                        
                            <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'featured_banner' );?>
                            <div class="slide-item" style="background-image: url('<?php echo $thumb['0'];?>">
                                
                                <div class="slide__caption js-first-slick clearfix d-none d-lg-block" data-animation="fadeInUp" data-animation-delay=".1s">
                                    <div class="number float-left">01</div>
                                    <h2 class="text"><?php the_title(); ?></h2>
                                    <span class="col-10 p-l-25 float-left"><?php the_excerpt(); ?></span>
                                </div>
                            </div>
                            
                          <?php endwhile; } wp_reset_query(); ?> 
                        </div>
                        <div class="slide-arrows">
                            <i class="zmdi zmdi-arrow-left js-slick-arrow-left prev"></i>
                            <i class="zmdi zmdi-arrow-right js-slick-arrow-right next"></i>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- END SLIDER-->



<?php get_footer(); ?>



