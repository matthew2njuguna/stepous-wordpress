<?php
/**
 * The main template file
 *
 * @package Purplefire
 * @subpackage Purplefire
 * @since Purplefire 1.0
 */

get_header(); ?>
<?php 
$slideCat = 2;
$teamCat = 3;
$membershipsCat = 4;
$newsCat = 5;
$testCat = 6;
?>
  <!-- #header -->
    <div class="zozo-top-anchor" id="section-top"></div>
    <div class="">
      <ul class="bxslider">

            <?php
              $args=array(
                'cat' =>$slideCat,
                'post_type' => 'post',
                'post_status' => 'publish',
                'orderby' => 'date','order' => 'DES',
                'posts_per_page'   => 5
                );
              $my_query = null;
              $my_query = new WP_Query($args);
              if( $my_query->have_posts() ) {
                 while ($my_query->have_posts()) : $my_query->the_post(); 
                 $post_id = $post->ID;
            ?>
            <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'featured_banner' );?>
            <li>
              <div class=""><img src="<?php echo $thumb['0'];?>" alt=""></div>
              <div class="caption center-align">
                <h6 class="yellow-text slidr_sub">
                    <?php the_field('slidr_sub'); ?>
                </h6>
                <h1 class="light grey-text text-lighten-3 upc"><?php the_title(); ?></h1>
                <p><?php the_excerpt(); ?></p>
                <a href="<?php the_permalink(); ?>" id="download-button" class="btn z-depth-custom2 stroke waves-effect waves-light teal lighten-1">Contact Our Attoneys</a>
              </div> 
            </li>  
            <?php endwhile;  } wp_reset_query(); ?>
      </ul>
    </div>
    <div class="zozo-main-wrapper">
      <div class="main-section" id="main">
        <!-- ============ Page Header ============ -->
        <!-- ============ Page Header Ends ============ -->
        <div class="main-fullwidth main-col-full" id="fullwidth">
          <section class="vc_row wpb_row vc_row-fluid vc_custom_1486812662402 vc-zozo-section typo-default">
            <div class="zozo-vc-main-row-inner vc-normal-section">
              <div class="container">
                <div class="row">
                  <div class="wpb_column vc_main_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 typo-default">
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">
                        <div class="zozo-feature-box feature-box-style style-title-top-icon style-sep-yes clearfix" id="feature-box-1">
                          <div class="grid-item">
                            <div class="grid-icon-box">
                              <div class="grid-box-inner grid-icon-box-wrapper grid-box-large grid-box-icon-none grid-icon-box-left">
                                <div class="grid-icon-box-title">
                                  <div class="grid-icon-wrapper icon-hv-color shape-icon-none">
                                    <i class="fa fa-user-secret grid-icon zozo-icon icon-none icon-skin-default icon-bg pattern-1 icon-large"></i>
                                  </div>
                                  <h4 class="grid-title">Prevent From Business Malpractices</h4>
                                </div>
                                <div class="clearfix"></div>
                                <div class="grid-icon-box-content">
                                  <div class="grid-desc">
                                    <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut.</p>
                                  </div>
                                  <div class="grid-button">
                                    <a class="btn btn-fbox-more" href="#" target=" _blank" title="">Read More</a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="wpb_column vc_main_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 typo-default">
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">
                        <div class="zozo-feature-box feature-box-style style-title-top-icon style-sep-yes clearfix" id="feature-box-2">
                          <div class="grid-item">
                            <div class="grid-icon-box">
                              <div class="grid-box-inner grid-icon-box-wrapper grid-box-large grid-box-icon-none grid-icon-box-left">
                                <div class="grid-icon-box-title">
                                  <div class="grid-icon-wrapper icon-hv-color shape-icon-none">
                                    <i class="fa fa-venus-double grid-icon zozo-icon icon-none icon-skin-default icon-bg pattern-1 icon-large"></i>
                                  </div>
                                  <h4 class="grid-title">Deals With Family &amp; Domestic Relationship</h4>
                                </div>
                                <div class="clearfix"></div>
                                <div class="grid-icon-box-content">
                                  <div class="grid-desc">
                                    <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut.</p>
                                  </div>
                                  <div class="grid-button">
                                    <a class="btn btn-fbox-more" href="#" target=" _blank" title="">Read More</a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="wpb_column vc_main_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 typo-default">
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">
                        <div class="zozo-feature-box feature-box-style style-title-top-icon style-sep-yes clearfix" id="feature-box-3">
                          <div class="grid-item">
                            <div class="grid-icon-box">
                              <div class="grid-box-inner grid-icon-box-wrapper grid-box-large grid-box-icon-none grid-icon-box-left">
                                <div class="grid-icon-box-title">
                                  <div class="grid-icon-wrapper icon-hv-color shape-icon-none">
                                    <i class="fa fa-gitlab grid-icon zozo-icon icon-none icon-skin-default icon-bg pattern-1 icon-large"></i>
                                  </div>
                                  <h4 class="grid-title">Is Body of Law, Offence Against Justice.</h4>
                                </div>
                                <div class="clearfix"></div>
                                <div class="grid-icon-box-content">
                                  <div class="grid-desc">
                                    <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut.</p>
                                  </div>
                                  <div class="grid-button">
                                    <a class="btn btn-fbox-more" href="#" target=" _blank" title="">Read More</a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="wpb_column vc_main_column vc_column_container vc_col-sm-6 vc_col-lg-3 vc_col-md-3 typo-default">
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">
                        <div class="zozo-feature-box feature-box-style style-title-top-icon style-sep-yes clearfix" id="feature-box-4">
                          <div class="grid-item">
                            <div class="grid-icon-box">
                              <div class="grid-box-inner grid-icon-box-wrapper grid-box-large grid-box-icon-none grid-icon-box-left">
                                <div class="grid-icon-box-title">
                                  <div class="grid-icon-wrapper icon-hv-color shape-icon-none">
                                    <i class="fa fa-recycle grid-icon zozo-icon icon-none icon-skin-default icon-bg pattern-1 icon-large"></i>
                                  </div>
                                  <h4 class="grid-title">Governments prohibit, except under licence.</h4>
                                </div>
                                <div class="clearfix"></div>
                                <div class="grid-icon-box-content">
                                  <div class="grid-desc">
                                    <p>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut.</p>
                                  </div>
                                  <div class="grid-button">
                                    <a class="btn btn-fbox-more" href="#" target=" _blank" title="">Read More</a>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section class="vc_row wpb_row vc_row-fluid vc-zozo-section typo-default">
            <div class="zozo-vc-main-row-inner vc-normal-section">
              <div class="container">
                <div class="row">
                  <div class="wpb_column vc_main_column vc_column_container vc_col-sm-12 typo-default">
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">
                        <div class="vc_separator wpb_content_element vc_separator_align_center vc_sep_width_100 vc_sep_pos_align_center vc_separator_no_text vc_sep_color_grey">
                          <span class="vc_sep_holder vc_sep_holder_l"><span class="vc_sep_line"></span></span><span class="vc_sep_holder vc_sep_holder_r"><span class="vc_sep_line"></span></span>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section class="vc_row wpb_row vc_row-fluid vc_custom_1486812782161 vc-zozo-section typo-default">
            <div class="zozo-vc-main-row-inner vc-normal-section">
              <div class="container">
                <div class="row">
                  <div class="wpb_column vc_main_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6 typo-default">
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">
                        <div class="zozo-parallax-header">
                          <div class="parallax-header content-style-default">
                            <h2 class="parallax-title text-left text-capitalize">About Us</h2>
                            <div class="parallax-desc default-style text-left">
                              <p>Muchemi &amp; Co. Advocates is a premier law firm providing a comprehensive range of
legal services and solutions. We offer diverse and vibrant practice encompassing the
more traditional fields of commercial law, as well as specialized areas, including a
fast growing reputation in banking and securities, commercial contracts and
client satisfaction.</p>
                              <p><a class="btn vc_general vc_btn3 vc_btn3-style-default" href="#" id="1023" target="_self" title="">More About Us</a></p>
                            
                            </div>
                          </div>
                        </div>
                        <div class="vc_row wpb_row vc_inner vc_row-fluid vc_custom_1486727143235">
                          <div class="zozo-vc-row-inner vc-inner-row-section clearfix margin-bottom-30">
                            <div class="wpb_column vc_column_inner vc_column_container vc_col-sm-4 typo-light">
                              <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                  <div class="zozo-feature-box feature-box-style theme-bg style-default-box style-sep-yes vc_custom_1486721277654 clearfix" id="feature-box-5">
                                    <div class="grid-item">
                                      <div class="grid-box-inner grid-text-center grid-box-medium grid-box-icon-none grid-shape-none">
                                        <div class="grid-icon-wrapper no-hover shape-icon-none">
                                          <i class="fa fa-pencil-square-o grid-icon zozo-icon icon-none icon-skin-light icon-bg pattern-1 icon-medium"></i>
                                        </div>
                                        <h5 class="grid-title-below grid-title">Request A Quote</h5>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="wpb_column vc_column_inner vc_column_container vc_col-sm-4 typo-light">
                              <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                  <div class="zozo-feature-box feature-box-style theme-bg style-default-box style-sep-yes vc_custom_1486721827638 clearfix" id="feature-box-6">
                                    <div class="grid-item">
                                      <div class="grid-box-inner grid-text-center grid-box-medium grid-box-icon-none grid-shape-none">
                                        <div class="grid-icon-wrapper no-hover shape-icon-none">
                                          <i class="fa fa-gavel grid-icon zozo-icon icon-none icon-skin-light icon-bg pattern-1 icon-medium"></i>
                                        </div>
                                        <h5 class="grid-title-below grid-title">Investigation</h5>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="wpb_column vc_column_inner vc_column_container vc_col-sm-4 typo-light">
                              <div class="vc_column-inner">
                                <div class="wpb_wrapper">
                                  <div class="zozo-feature-box feature-box-style theme-bg style-default-box style-sep-yes vc_custom_1486721863688 clearfix" id="feature-box-7">
                                    <div class="grid-item">
                                      <div class="grid-box-inner grid-text-center grid-box-medium grid-box-icon-none grid-shape-none">
                                        <div class="grid-icon-wrapper no-hover shape-icon-none">
                                          <i class="fa fa-magic grid-icon zozo-icon icon-none icon-skin-light icon-bg pattern-1 icon-medium"></i>
                                        </div>
                                        <h5 class="grid-title-below grid-title">Legal Proceeding</h5>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="zozo-parallax-header">
                          <div class="parallax-header content-style-default">
                            <h2 class="parallax-title text-left text-capitalize">Scope of Service & Specialty</h2>
                            <div class="parallax-desc default-style text-left">
                              <p>
At Muchemi &amp; Co. we are committed to offering the highest quality of legal services
to our clients. We offer a comprehensive range of legal services.
The firm has a dynamic team offering quality personalized service.</p>
                              <p><a class="btn vc_general vc_btn3 vc_btn3-style-default" href="#" id="1023" target="_self" title="">Keep reading</a></p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="timeline-custom-style wpb_column vc_main_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6 typo-default">
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">
                        <div class="zozo-parallax-header">
                          <div class="parallax-header content-style-default">
                            <h2 class="parallax-title text-left text-capitalize">Our History</h2>
                          </div>
                        </div>
                        <div class="zozo-timeline-wrapper vc-timeline-items">
                          <div class="zozo-timeline-item timeline-style-2 timeline-align-left timeline-skin-light">
                            <div class="zozo-time-separator-text">
                              <div class="zozo-time-separator-inner">
                                <h4 class="timeline-sep-text">1965</h4>
                              </div>
                            </div>
                            <div class="timeline-separator-outer"></div>
                            <div class="timeline-content-wrapper">
                              <h4 class="timeline-title">Started at Nairobi</h4>
                              <div class="timeline-desc">
                                <p>Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae sem quam semper libero adipiscing sem neque.</p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="zozo-timeline-wrapper vc-timeline-items">
                          <div class="zozo-timeline-item timeline-style-2 timeline-align-left timeline-skin-light">
                            <div class="zozo-time-separator-text">
                              <div class="zozo-time-separator-inner">
                                <h4 class="timeline-sep-text">1972</h4>
                              </div>
                            </div>
                            <div class="timeline-separator-outer"></div>
                            <div class="timeline-content-wrapper">
                              <h4 class="timeline-title">Best company of the Year</h4>
                              <div class="timeline-desc">
                                <p>Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae sem quam semper libero adipiscing sem neque.</p>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="zozo-timeline-wrapper vc-timeline-items">
                          <div class="zozo-timeline-item timeline-style-2 timeline-align-left timeline-skin-light">
                            <div class="zozo-time-separator-text">
                              <div class="zozo-time-separator-inner">
                                <h4 class="timeline-sep-text">1978</h4>
                              </div>
                            </div>
                            <div class="timeline-separator-outer"></div>
                            <div class="timeline-content-wrapper">
                              <h4 class="timeline-title">Opening new office</h4>
                              <div class="timeline-desc">
                                <p>Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae sem quam semper libero adipiscing sem neque.</p>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section class="vc_row wpb_row vc_row-fluid vc_custom_1486793146962 vc_row-has-fill vc-zozo-section typo-dark bg-style grey-wrapper" data-vc-full-width="true" data-vc-full-width-init="false">
            <div class="zozo-vc-main-row-inner vc-normal-section vc-match-height-row">
              <div class="wpb_column vc_main_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6 vc_col-has-fill typo-light">
                <div class="vc_column-inner vc_custom_1486791078226">
                  <div class="wpb_wrapper">
                    <h2 class="vc_custom_heading vc_custom_1486791281108" style="color: #ffffff;text-align: center">What Our Client Says About<br>
                    Our Firm.</h2>
                    <div class="zozo-testimonial-slider-wrapper testimonial-light">
                      <ul class="bxslider2 testimonial-carousel-slider" data-autoplay="true" data-autoplay-timeout="5000" data-items="1" data-items-mobile-landscape="1" data-items-mobile-portrait="1" data-items-tablet="1" data-loop="false" data-navigation="false" data-pagination="true" data-slideby="1" id="zozo-testimonial-slider1">
                       


        <?php
              $args=array(
                'cat' =>$testCat,
                'post_type' => 'post',
                'post_status' => 'publish',
                'orderby' => 'date','order' => 'DES',
                'posts_per_page'   => 5
                );
              $my_query = null;
              $my_query = new WP_Query($args);
              if( $my_query->have_posts() ) {
                 while ($my_query->have_posts()) : $my_query->the_post(); 
                 $post_id = $post->ID;
            ?>
            <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'team_thumb' );?>
                 <li class="testimonial-item tstyle-style-3 text-center">
                          <div class="author-info-box">
                            <div class="testimonial-img"><img alt="Katherin Tresa" class="img-responsive" height="85" src="<?php echo $thumb['0'];?>" width="85"></div>
                            <div class="author-details">
                              <p><span class="testimonial-author-name"><a href="http://themes.zozothemes.com/lawyer/testimonial/katherin-tresa/" title="Katherin Tresa"><?php the_title(); ?></a></span></p>
                              <p class="author-designation-info"><span class="testimonial-author-designation"><?php the_field('designation'); ?></span><span class="testimonial-author-company"><?php the_field('company'); ?></span></p>
                            </div>
                          </div>
                          <div class="testimonial-content">
                            <blockquote>
                              <p><?php the_content(); ?></p>
                            </blockquote>
                          </div>
                        </li>
            <?php endwhile;  } wp_reset_query(); ?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="wpb_column vc_main_column vc_column_container vc_col-sm-12 vc_col-lg-6 vc_col-md-6 typo-default">
                <div class="vc_column-inner vc_custom_1486547490052">
                  <div class="wpb_wrapper">
                    <h2 class="vc_custom_heading vc_custom_1486731452890" style="text-align: left">DEALS WITH FAMILY &amp; RELATIONSHIP!</h2>
                    <div class="zozo-feature-box feature-box-style style-default-box style-sep-yes clearfix" id="feature-box-8">
                      <div class="grid-item">
                        <div class="grid-box-inner grid-text-left grid-box-medium grid-box-icon-circle grid-icon-shape grid-shape-none">
                          <div class="grid-icon-wrapper no-hover shape-icon-circle">
                            <i class="fa fa-balance-scale grid-icon zozo-icon icon-shape icon-circle icon-skin-default icon-bg pattern-1 icon-medium"></i>
                          </div>
                          <div class="grid-content-wrapper">
                            <h4 class="grid-title">Fight For Justice</h4>
                            <div class="grid-desc">
                              <p>Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Etiam rhoncus Maecenas tempus, tellus eget condimentum.</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="zozo-feature-box feature-box-style style-default-box style-sep-yes clearfix" id="feature-box-9">
                      <div class="grid-item">
                        <div class="grid-box-inner grid-text-left grid-box-medium grid-box-icon-circle grid-icon-shape grid-shape-none">
                          <div class="grid-icon-wrapper no-hover shape-icon-circle">
                            <i class="fa fa-suitcase grid-icon zozo-icon icon-shape icon-circle icon-skin-default icon-bg pattern-1 icon-medium"></i>
                          </div>
                          <div class="grid-content-wrapper">
                            <h4 class="grid-title">Best Case Stratergy</h4>
                            <div class="grid-desc">
                              <p>Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Etiam rhoncus Maecenas tempus, tellus eget condimentum.</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="zozo-feature-box feature-box-style style-default-box style-sep-yes clearfix" id="feature-box-10">
                      <div class="grid-item">
                        <div class="grid-box-inner grid-text-left grid-box-medium grid-box-icon-circle grid-icon-shape grid-shape-none">
                          <div class="grid-icon-wrapper no-hover shape-icon-circle">
                            <i class="fa fa-male grid-icon zozo-icon icon-shape icon-circle icon-skin-default icon-bg pattern-1 icon-medium"></i>
                          </div>
                          <div class="grid-content-wrapper">
                            <h4 class="grid-title">Experienced Attorneys</h4>
                            <div class="grid-desc">
                              <p>Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Etiam rhoncus Maecenas tempus, tellus eget condimentum.</p>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <div class="vc_row-full-width vc_clearfix"></div>
          <section class="vc_row wpb_row vc_row-fluid vc-zozo-section typo-default">
            <div class="zozo-vc-main-row-inner vc-normal-section">
              <div class="container">
                <div class="row">
                  <div class="wpb_column vc_main_column vc_column_container vc_col-sm-12 typo-default">
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">
                        <div class="zozo-parallax-header">
                          <div class="parallax-header content-style-default">
                            <h2 class="parallax-title">Meet Our Attorneys</h2>
                            <div class="parallax-desc default-style">
                              <p>Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero.</p>
                            </div>
                          </div>
                        </div>
                        <div class="zozo-team-slider-wrapper team-box_type">
                          <div class="bxslider1">
                            <?php
                              $args=array(
                                'cat' =>$teamCat,
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'orderby' => 'date','order' => 'DES',
                                'posts_per_page'   => 5
                                );
                              $my_query = null;
                              $my_query = new WP_Query($args);
                              if( $my_query->have_posts() ) {
                                 while ($my_query->have_posts()) : $my_query->the_post(); 
                                 $post_id = $post->ID;
                            ?>
                          
                            <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'team_thumb' );?>
                            <div class="slide">
                              <div class="team-item-img"><img src="<?php echo $thumb['0'];?>" alt=""></div>
                              <div class="team-content">
                                <h5 class="team-member-name"><a href="<?php the_permalink(); ?>" title="Salman Amir"><?php the_title(); ?></a></h5>
                                <p><span class="team-member-designation"><?php the_excerpt(); ?></span></p>
                                <div class="zozo-team-social">
                                  <ul class="zozo-social-icons soc-icon-transparent zozo-team-social-list">
                                    <li class="facebook">
                                      <a href="#" target="_self"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li class="twitter">
                                      <a href="#" target="_self"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li class="google-plus">
                                      <a href="#" target="_self"><i class="fa fa-google-plus"></i></a>
                                    </li>
                                    <li class="linkedin">
                                      <a href="#" target="_self"><i class="fa fa-linkedin"></i></a>
                                    </li>
                                    <li class="pinterest">
                                      <a href="#" target="_self"><i class="fa fa-pinterest"></i></a>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                            </div>
                            <?php endwhile;  } wp_reset_query(); ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section class="vc_row wpb_row vc_row-fluid vc_custom_1486790743530 vc-zozo-section typo-default">
            <div class="zozo-vc-main-row-inner vc-normal-section">
              <div class="container">
                <div class="row">
                  <div class="wpb_column vc_main_column vc_column_container vc_col-sm-12 typo-default">
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">
                        <div class="zozo-parallax-header">
                          <div class="parallax-header content-style-default">
                            <h2 class="parallax-title">Our Latest News</h2>
                            <div class="parallax-desc default-style">
                              <p>Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero.</p>
                            </div>
                          </div>
                        </div>
                        <div class="zozo-latest-posts-wrapper">
                          <div class="zozo-owl-carousel owl-carousel latest-posts-slider-wrapper" data-autoplay="true" data-autoplay-timeout="3000" data-items="3" data-items-mobile-landscape="1" data-items-mobile-portrait="1" data-items-tablet="1" data-loop="true" data-margin="30" data-navigation="false" data-pagination="false" data-slideby="1" id="zozo-latest-posts-slider1">
                            <div class="latest-posts-slider post-683 post type-post status-publish format-standard has-post-thumbnail hentry category-family category-financial category-law tag-family tag-financial" id="post-683">
                              <div class="post-inner-wrapper posts-inner-container clearfix">
                                <div class="entry-meta-top-wrapper"></div>
                                <div class="entry-header">
                                  <h2 class="entry-title"><a href="http://themes.zozothemes.com/lawyer/2016/10/17/steps-needed-for-putting-lawyer-into-action/" rel="bookmark" title="Steps Needed For Putting Lawyer Into Action.">Steps Needed For Putting Lawyer Into Action.</a></h2>
                                </div>
                                <div class="post-featured-image only-image">
                                  <div class="entry-thumbnail-wrapper">
                                    <div class="entry-thumbnail">
                                      <a class="post-img" href="http://themes.zozothemes.com/lawyer/2016/10/17/steps-needed-for-putting-lawyer-into-action/" title="Steps Needed For Putting Lawyer Into Action."><img alt="" class="attachment-lawyer-blog-medium size-lawyer-blog-medium wp-post-image" height="370" src="http://themes.zozothemes.com/lawyer/wp-content/uploads/sites/3/2016/10/blog6-570x370.jpg" width="570"></a>
                                    </div>
                                  </div>
                                </div>
                                <div class="posts-content-container">
                                  <div class="entry-summary">
                                    <p>Maecenas iaculis fringilla magna sit amet volutpat. Proin vestibulum, ex quis imperdiet tempus, nunc turpis condimentum est, at</p>
                                  </div>
                                  <div class="entry-meta-wrapper">
                                    <div class="entry-meta">
                                      <div class="post-author">
                                        <img alt='' class='avatar avatar-40 photo' height='40' src='http://0.gravatar.com/avatar/c3450b43a49515593272437e9a374ad3?s=40&#038;d=mm&#038;r=g' srcset='http://0.gravatar.com/avatar/c3450b43a49515593272437e9a374ad3?s=80&amp;d=mm&amp;r=g 2x' width='40'>
                                        <h6 class="post-author-name"><a href="http://themes.zozothemes.com/lawyer/author/zozo_user/" rel="author" title="Posts by zozo_user">zozo_user</a></h6>
                                      </div>
                                    </div>
                                    <div class="read-more">
                                      <a class="btn-more read-more-link" href="http://themes.zozothemes.com/lawyer/2016/10/17/steps-needed-for-putting-lawyer-into-action/" title="Steps Needed For Putting Lawyer Into Action.">Continue Reading</a>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="latest-posts-slider post-681 post type-post status-publish format-standard has-post-thumbnail hentry category-attorneys category-firm category-industrial tag-attorneys tag-firm tag-industrial" id="post-681">
                              <div class="post-inner-wrapper posts-inner-container clearfix">
                                <div class="entry-meta-top-wrapper"></div>
                                <div class="entry-header">
                                  <h2 class="entry-title"><a href="http://themes.zozothemes.com/lawyer/2016/10/17/now-is-the-time-to-know-about-lawyer/" rel="bookmark" title="Now Is The Time To Know About Lawyer.">Now Is The Time To Know About Lawyer.</a></h2>
                                </div>
                                <div class="post-featured-image only-image">
                                  <div class="entry-thumbnail-wrapper">
                                    <div class="entry-thumbnail">
                                      <a class="post-img" href="http://themes.zozothemes.com/lawyer/2016/10/17/now-is-the-time-to-know-about-lawyer/" title="Now Is The Time To Know About Lawyer."><img alt="" class="attachment-lawyer-blog-medium size-lawyer-blog-medium wp-post-image" height="370" src="http://themes.zozothemes.com/lawyer/wp-content/uploads/sites/3/2016/10/blog5-570x370.jpg" width="570"></a>
                                    </div>
                                  </div>
                                </div>
                                <div class="posts-content-container">
                                  <div class="entry-summary">
                                    <p>Maecenas iaculis fringilla magna sit amet volutpat. Proin vestibulum, ex quis imperdiet tempus, nunc turpis condimentum est, at</p>
                                  </div>
                                  <div class="entry-meta-wrapper">
                                    <div class="entry-meta">
                                      <div class="post-author">
                                        <img alt='' class='avatar avatar-40 photo' height='40' src='http://0.gravatar.com/avatar/c3450b43a49515593272437e9a374ad3?s=40&#038;d=mm&#038;r=g' srcset='http://0.gravatar.com/avatar/c3450b43a49515593272437e9a374ad3?s=80&amp;d=mm&amp;r=g 2x' width='40'>
                                        <h6 class="post-author-name"><a href="http://themes.zozothemes.com/lawyer/author/zozo_user/" rel="author" title="Posts by zozo_user">zozo_user</a></h6>
                                      </div>
                                    </div>
                                    <div class="read-more">
                                      <a class="btn-more read-more-link" href="http://themes.zozothemes.com/lawyer/2016/10/17/now-is-the-time-to-know-about-lawyer/" title="Now Is The Time To Know About Lawyer.">Continue Reading</a>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="latest-posts-slider post-679 post type-post status-publish format-standard has-post-thumbnail hentry category-family category-industrial category-law tag-family tag-industrial tag-law" id="post-679">
                              <div class="post-inner-wrapper posts-inner-container clearfix">
                                <div class="entry-meta-top-wrapper"></div>
                                <div class="entry-header">
                                  <h2 class="entry-title"><a href="http://themes.zozothemes.com/lawyer/2016/10/17/lessons-that-will-teach-you-about-lawyer/" rel="bookmark" title="Lessons That Will Teach You About Lawyer.">Lessons That Will Teach You About Lawyer.</a></h2>
                                </div>
                                <div class="post-featured-image only-image">
                                  <div class="entry-thumbnail-wrapper">
                                    <div class="entry-thumbnail">
                                      <a class="post-img" href="http://themes.zozothemes.com/lawyer/2016/10/17/lessons-that-will-teach-you-about-lawyer/" title="Lessons That Will Teach You About Lawyer."><img alt="" class="attachment-lawyer-blog-medium size-lawyer-blog-medium wp-post-image" height="370" src="http://themes.zozothemes.com/lawyer/wp-content/uploads/sites/3/2016/10/blog4-570x370.jpg" width="570"></a>
                                    </div>
                                  </div>
                                </div>
                                <div class="posts-content-container">
                                  <div class="entry-summary">
                                    <p>Maecenas iaculis fringilla magna sit amet volutpat. Proin vestibulum, ex quis imperdiet tempus, nunc turpis condimentum est, at</p>
                                  </div>
                                  <div class="entry-meta-wrapper">
                                    <div class="entry-meta">
                                      <div class="post-author">
                                        <img alt='' class='avatar avatar-40 photo' height='40' src='http://0.gravatar.com/avatar/c3450b43a49515593272437e9a374ad3?s=40&#038;d=mm&#038;r=g' srcset='http://0.gravatar.com/avatar/c3450b43a49515593272437e9a374ad3?s=80&amp;d=mm&amp;r=g 2x' width='40'>
                                        <h6 class="post-author-name"><a href="http://themes.zozothemes.com/lawyer/author/zozo_user/" rel="author" title="Posts by zozo_user">zozo_user</a></h6>
                                      </div>
                                    </div>
                                    <div class="read-more">
                                      <a class="btn-more read-more-link" href="http://themes.zozothemes.com/lawyer/2016/10/17/lessons-that-will-teach-you-about-lawyer/" title="Lessons That Will Teach You About Lawyer.">Continue Reading</a>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="latest-posts-slider post-677 post type-post status-publish format-standard has-post-thumbnail hentry category-attorneys category-financial category-law tag-financial tag-law" id="post-677">
                              <div class="post-inner-wrapper posts-inner-container clearfix">
                                <div class="entry-meta-top-wrapper"></div>
                                <div class="entry-header">
                                  <h2 class="entry-title"><a href="http://themes.zozothemes.com/lawyer/2016/10/17/the-truth-about-lawyer-is-to-be-revealed/" rel="bookmark" title="The Truth About Lawyer Is To Be Revealed.">The Truth About Lawyer Is To Be Revealed.</a></h2>
                                </div>
                                <div class="post-featured-image only-image">
                                  <div class="entry-thumbnail-wrapper">
                                    <div class="entry-thumbnail">
                                      <a class="post-img" href="http://themes.zozothemes.com/lawyer/2016/10/17/the-truth-about-lawyer-is-to-be-revealed/" title="The Truth About Lawyer Is To Be Revealed."><img alt="" class="attachment-lawyer-blog-medium size-lawyer-blog-medium wp-post-image" height="370" src="http://themes.zozothemes.com/lawyer/wp-content/uploads/sites/3/2016/10/blog3-570x370.jpg" width="570"></a>
                                    </div>
                                  </div>
                                </div>
                                <div class="posts-content-container">
                                  <div class="entry-summary">
                                    <p>Maecenas iaculis fringilla magna sit amet volutpat. Proin vestibulum, ex quis imperdiet tempus, nunc turpis condimentum est, at</p>
                                  </div>
                                  <div class="entry-meta-wrapper">
                                    <div class="entry-meta">
                                      <div class="post-author">
                                        <img alt='' class='avatar avatar-40 photo' height='40' src='http://0.gravatar.com/avatar/c3450b43a49515593272437e9a374ad3?s=40&#038;d=mm&#038;r=g' srcset='http://0.gravatar.com/avatar/c3450b43a49515593272437e9a374ad3?s=80&amp;d=mm&amp;r=g 2x' width='40'>
                                        <h6 class="post-author-name"><a href="http://themes.zozothemes.com/lawyer/author/zozo_user/" rel="author" title="Posts by zozo_user">zozo_user</a></h6>
                                      </div>
                                    </div>
                                    <div class="read-more">
                                      <a class="btn-more read-more-link" href="http://themes.zozothemes.com/lawyer/2016/10/17/the-truth-about-lawyer-is-to-be-revealed/" title="The Truth About Lawyer Is To Be Revealed.">Continue Reading</a>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="latest-posts-slider post-675 post type-post status-publish format-standard has-post-thumbnail hentry category-family category-firm category-law tag-family tag-industrial" id="post-675">
                              <div class="post-inner-wrapper posts-inner-container clearfix">
                                <div class="entry-meta-top-wrapper"></div>
                                <div class="entry-header">
                                  <h2 class="entry-title"><a href="http://themes.zozothemes.com/lawyer/2016/10/17/simple-guidance-for-you-in-lawyer/" rel="bookmark" title="Simple Guidance For You In Lawyer.">Simple Guidance For You In Lawyer.</a></h2>
                                </div>
                                <div class="post-featured-image only-image">
                                  <div class="entry-thumbnail-wrapper">
                                    <div class="entry-thumbnail">
                                      <a class="post-img" href="http://themes.zozothemes.com/lawyer/2016/10/17/simple-guidance-for-you-in-lawyer/" title="Simple Guidance For You In Lawyer."><img alt="" class="attachment-lawyer-blog-medium size-lawyer-blog-medium wp-post-image" height="370" src="http://themes.zozothemes.com/lawyer/wp-content/uploads/sites/3/2016/10/blog2-570x370.jpg" width="570"></a>
                                    </div>
                                  </div>
                                </div>
                                <div class="posts-content-container">
                                  <div class="entry-summary">
                                    <p>Maecenas iaculis fringilla magna sit amet volutpat. Proin vestibulum, ex quis imperdiet tempus, nunc turpis condimentum est, at</p>
                                  </div>
                                  <div class="entry-meta-wrapper">
                                    <div class="entry-meta">
                                      <div class="post-author">
                                        <img alt='' class='avatar avatar-40 photo' height='40' src='http://0.gravatar.com/avatar/c3450b43a49515593272437e9a374ad3?s=40&#038;d=mm&#038;r=g' srcset='http://0.gravatar.com/avatar/c3450b43a49515593272437e9a374ad3?s=80&amp;d=mm&amp;r=g 2x' width='40'>
                                        <h6 class="post-author-name"><a href="http://themes.zozothemes.com/lawyer/author/zozo_user/" rel="author" title="Posts by zozo_user">zozo_user</a></h6>
                                      </div>
                                    </div>
                                    <div class="read-more">
                                      <a class="btn-more read-more-link" href="http://themes.zozothemes.com/lawyer/2016/10/17/simple-guidance-for-you-in-lawyer/" title="Simple Guidance For You In Lawyer.">Continue Reading</a>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <section class="vc_row wpb_row vc_row-fluid vc-zozo-section typo-default bg-style grey-wrapper">
            <div class="zozo-vc-main-row-inner vc-normal-section">
              <div class="container">
                <div class="row">
                  <div class="wpb_column vc_main_column vc_column_container vc_col-sm-12 typo-default">
                    <div class="vc_column-inner">
                      <div class="wpb_wrapper">
                        <div class="zozo-parallax-header">
                          <div class="parallax-header content-style-default">
                            <h2 class="parallax-title">MEMBERSHIPS</h2>
                            <div class="parallax-desc default-style">
                              <p>The managing advocate is a member of the following Professional organizations;</p>
                            </div>
                          </div>
                        </div>
                        <div class="zozo-team-slider-wrapper team-box_type clientr">
                          <div class="clientcarousel">
                            <?php
                              $args=array(
                                'cat' =>$membershipsCat,
                                'post_type' => 'post',
                                'post_status' => 'publish',
                                'orderby' => 'date','order' => 'DES',
                                'posts_per_page'   => 5
                                );
                              $my_query = null;
                              $my_query = new WP_Query($args);
                              if( $my_query->have_posts() ) {
                                 while ($my_query->have_posts()) : $my_query->the_post(); 
                                 $post_id = $post->ID;
                            ?>
                          
                            <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'memberships_thumb' );?>
                            <div class="slide">
                              <div class="client-item white pad1em">
                                <a href="#" target="_blank"><img src="<?php echo $thumb['0'];?>" alt=""></a>
                              </div>
                            </div>
                            <?php endwhile;  } wp_reset_query(); ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div><!-- #fullwidth -->
      </div><!-- #main -->
<?php get_footer(); ?>

