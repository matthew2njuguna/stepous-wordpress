<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>


        <!-- PAGE TITLE-->
        <section class="page-title js-parallax-scroll p-t-175 p-b-175 error" style="background-image: url(<?php echo esc_url( get_template_directory_uri() ); ?>/images/homebanner.png); background-repeat:no-repeat;">
            
            <div class="page-title__inner col-lg-6 text-left text-white p-l-100">
                <span class="label posts__cat-label">Woopsie!</span>
				<h2 class="text-left">Error 404</h2>
				<p class="p-t-10"><?php _e( 'It looks like nothing was found at this location. Maybe try a search?', 'twentyfifteen' ); ?></p>
				<div class="p-t-40"><?php get_search_form(); ?></div>
            </div>
        </section>
        <!-- END PAGE TITLE-->


<?php get_footer(); ?>
