<?php
/**
 * Template Name: Contact Us Page Template 
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package ous
 */

  //response generation function

  $response = "";

  //function to generate response
  function my_contact_form_generate_response($type, $message){

    global $response;

    if($type == "success") $response = "<div class='success wow fadeInUp' data-wow-delay='.9s'>{$message}</div>";
    else $response = "<div class='error wow fadeInUp' data-wow-delay='.9s'>{$message}</div>";

  }

  //response messages
  $not_human       = "Human verification incorrect.";
  $missing_content = "Please supply all information.";
  $email_invalid   = "Email Address Invalid.";
  $message_unsent  = "Message was not sent. Try Again.";
  $message_sent    = "Thanks! Your message has been sent.";

  //user posted variables
  $name = $_POST['message_name'];
  $email = $_POST['message_email'];
  $message = $_POST['message_text'];
  $human = $_POST['message_human'];

  //php mailer variables
  $to = get_option('admin_email');
  $subject = "Someone sent a message from ".get_bloginfo('name');
  $headers = 'From: '. $email . "\r\n" .
    'Reply-To: ' . $email . "\r\n";

  if(!$human == 0){
    if($human != 2) my_contact_form_generate_response("error", $not_human); //not human!
    else {

      //validate email
      if(!filter_var($email, FILTER_VALIDATE_EMAIL))
        my_contact_form_generate_response("error", $email_invalid);
      else //email is valid
      {
        //validate presence of name and message
        if(empty($name) || empty($message)){
          my_contact_form_generate_response("error", $missing_content);
        }
        else //ready to go!
        {
          $sent = wp_mail($to, $subject, strip_tags($message), $headers);
          if($sent) my_contact_form_generate_response("success", $message_sent); //message sent!
          else my_contact_form_generate_response("error", $message_unsent); //message wasn't sent
        }
      }
    }
  }
  else if ($_POST['submitted']) my_contact_form_generate_response("error", $missing_content);

get_header(); ?>



  
  <div class="container recent-blog subtr-brdcrbs">
            <div class="recent-blog__title-wrap clearfix m-b-90">
                <h2 class="recent-blog__title float-right wow fadeInUp" data-wow-delay=".2s"><?php the_title(); ?></h2>
            </div>
        </div>

        <!-- CONTACT-->
        <div class="container">
        <section class="contact p-t-100 p-b-50">
            <div class="container">
            </div>
            <div class="col-lg-12">
                <div class="row">
                    
                    <div class="col-md-7 col-sm-12 black">
                        <!-- <div class="col-md-12"> </div> -->
                        <div id="respond" class="col-md-12 p-30 cont p-0sm">
                            <h3 class="title title--dark title--semibold text-white wow fadeInUp" data-wow-delay=".8s">send us message</h3>
                       
                            <?php     // TO SHOW THE PAGE CONTENTS
                                while ( have_posts() ) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
                                <p class="m-b-35 wow fadeInUp" data-wow-delay=".9s"><?php the_content(); ?></p> <!-- Page Content -->
                                <?php
                            endwhile; //resetting the page loop
                            wp_reset_query(); //resetting the page query
                            ?>
                       
                            <?php echo $response; ?>
                            <form class="form-contact" method="POST" action="<?php the_permalink(); ?>">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <input class="au-input au-input--full m-b-30" type="text" name="message_name" value="<?php echo esc_attr($_POST['message_name']); ?>" placeholder="Your Name">
                                    </div>
                                    <div class="col-lg-6">
                                        <input class="au-input au-input--full m-b-30" type="email" name="message_email" value="<?php echo esc_attr($_POST['message_email']); ?>" placeholder="Your Email">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <textarea class="au-input au-textfield au-input--full" placeholder="Your Message" name="message_text"><?php echo esc_textarea($_POST['message_text']); ?></textarea>
                                   
                                        <div class="col-md-6 m-t-20 p-0 pull-left human">
                                            <label for="message_human">Human Verification: <span>*</span> <br><input type="text" class="au-input" style="width: 60px;" name="message_human"> + 3 = 5</label>
                                            <input type="hidden" name="submitted" value="1">
                                            </div>
                                            <input class="col-sm-12 col-md-4 au-btn au-btn--dark au-btn--p70 m-t-30 pull-right" type="submit">
                                        
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-md-5 float-right col-sm-12 p-t-40">
                            <h3 class="title title--dark title--semibold wow fadeInUp p-b-50" data-wow-delay=".8s">Contact Infomation</h3>
                            <div class="row">
                                
                                <div class=" col-md-12 wow fadeInUp contact-info" data-wow-delay=".3s">
                                    <p>
                                        <i class="fas fa-home"></i><?php the_field('adress'); ?></p>
                                </div>
                                <div class="col-md-12 wow fadeInUp contact-info" data-wow-delay=".4s">
                                    <p>
                                        <i class="fas fa-phone"></i><?php the_field('phone'); ?></p>
                                </div>
                                <div class="col-md-12 wow fadeInUp contact-info" data-wow-delay=".5s">
                                    <p>
                                        <i class="fas fa-envelope"></i><?php the_field('email'); ?></p>
                                </div>
                        </div>
                    </div>
                    <!-- <div class="col-md-6 p-0">
                        <div class="contact__image">   
                            <div class="map-wrapper js-google-map" data-makericon="images/icon/marker.png" data-makers="[[&quot;Umber&quot;, &quot;Now that you visited our website,&lt;br&gt; how about checking out our office too?&quot;, -1.292066, 36.821946]]">
                                <div class="map__holder js-map-holder" id="map"></div>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </section>
                            </div>
        <!-- END CONTACT-->
        <!-- END CONTACT 2-->


    <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAEmXgQ65zpsjsEAfNPP9mBAz-5zjnIZBw"></script> -->
    <!-- <script src="<?php bloginfo('stylesheet_directory')?>/js/theme-map.js"></script> -->

<?php get_footer(); ?>

