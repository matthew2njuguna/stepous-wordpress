<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package chrisc
 */

get_header(); ?>

        <!-- PAGE TITLE-->
        <section class="page-title js-parallax-scroll p-t-175 p-b-175" style="background-image: url(<?php echo esc_url( get_template_directory_uri() ); ?>/images/homebanner.png); background-repeat:no-repeat;">
            
            <div class="page-title__inner col-lg-6 text-left text-white p-l-100">
            <?php the_breadcrumb(); ?>
                <h2 class="text-left"><?php single_cat_title('Checkout Our '); ?></h2>
                <p class="p-t-30">Intentionally relevant. All non-denom. Lean into some totally relational placeholder text for your next website project. Churchly ipsum dolor amet […]</p>
            </div>
        </section>
        <!-- END PAGE TITLE-->

        <!-- SERVICE-->
        <section class="service-3 bg-f8 p-t-70 p-b-40">
            <div class="container">
                <div class="row">
                <?php while (have_posts()) : the_post(); ?>
                    <div class=" service-3-item service-3-item--img-left clearfix">
                        <div class="image">
                            <a href="<?php the_permalink(); ?>">
                            <?php if ( function_exists( 'add_theme_support' ) ) the_post_thumbnail('upcoming_thumb'); ?>
                            </a>
                        </div>
                        <div class="content">
                            <h2 class="title title--num1">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </h2>
                            <p><?php the_excerpt(); ?></p>
                        </div>
                    </div>
                <?php endwhile; // end of the loop. ?>
                </div>
            </div>
        </section>
        <!-- END SERVICE-->






<?php get_footer(); ?>