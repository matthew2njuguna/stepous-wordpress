<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package beleen
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>



<?php if ( have_posts() ) : ?>
        <?php while ( have_posts() ) : the_post(); ?>  

        <?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'singel_banner' ); ?>  
        <!-- PAGE TITLE-->
        <section class="page-title js-parallax-scroll p-t-175 p-b-175" style="background: url('<?php echo $backgroundImg[0]?>') center center / cover no-repeat fixed;">
        <div class="bg-overlay bg-overlay--opa30"></div>
            <div class="page-title__inner col-lg-6 text-left text-white p-l-100">
                <?php the_breadcrumb(); ?>
                <h2 class="text-left"><?php the_title(); ?></h2>
                <p class="p-t-30"><?php the_field('intro'); ?></p>
            </div>
        </section>
        <!-- END PAGE TITLE-->

        <!-- PROJECT INFO 3-->
        <section class="project-info-1 project-info-3 p-t-50 p-b-70">
            <div class="container">
                
            <p class="p-t-30 p-b-30"><b><em><?php the_field('first_paragraph'); ?></em></b></p>
                <div class="project-image-wrap">
                    <div class="row">
                        <div class="col-lg-7">
                            <div class="project-image">
                                        <img src="<?php the_field('introbold'); ?>" alt="">
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="project-image">
                                        <img src="<?php the_field('info_image_two'); ?>" alt="">
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="project-image">
                                        <img src="<?php the_field('info_image_three'); ?>" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="project-image">
                                        <img src="<?php the_field('info_image_four'); ?>" alt="">
                            </div>
                            <div class="project-image">
                                        <img src="<?php the_field('info_image_five'); ?>" alt="">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        
                    <div class="project-info__text p-t-30 col-lg-7"><?php the_content(); ?></div>
                    <div class="col-lg-4 p-t-30 ml-auto"><?php get_sidebar(); ?></div>
</div>
                </div>
                <div class="project-info-arrows">
                    <a class="prev" href="#">
                        <i class="zmdi zmdi-arrow-left"></i>prev Read</a>
                    <a class="next" href="#">next Read
                        <i class="zmdi zmdi-arrow-right"></i>
                    </a>
                </div>
            </div>
        </section>
        <!-- END PROJECT INFO 3-->

        
        <?php endwhile; else : ?>
                                                <p><?php esc_html_e( 'Sorry, no posts matched your criteria.' ); ?></p>
                                            <?php endif; ?>




    
								
<?php get_footer(); ?>
