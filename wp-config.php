<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'ous');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'S+`CTU6rm5&s+VDdSjw-p612LjN9wO/JNdzyvo~~XE$$6GEc:@Ry5<b*4.>iVQVM');
define('SECURE_AUTH_KEY',  '_IK-6RW7=sE&gIOzCvcFdcks*;2,n>voy+*dyF=%nOSzcD(,`Ac#,I+/>V57dff}');
define('LOGGED_IN_KEY',    '6eh1Ny0tEPC,Ih![$pUYMR#C5m#U0xZ`]C~J+(RI2{So;Z:)(f~YO8g<Dw_}$ix0');
define('NONCE_KEY',        '][2?L,lk-[tq1] sf_^kR},Y89yf]YuZ7^LOI|DYvqk$8Go]Tsde1!oW[E)eU$Xu');
define('AUTH_SALT',        'jP?h<;=@~c0f|V2x(-u?wj,<._Q@&sY^6S}SqZ?()fJ{IM!HVtI5<7Dnp+0Kp)#t');
define('SECURE_AUTH_SALT', 'Fxq{KW!g9_e1ZR([OX(9yos<5:QR?Din5_`A~e<{bKxWpdZX@secaz5vz,W,[Ek}');
define('LOGGED_IN_SALT',   '.L.S{;!M_M}bO]sI`x/mzOM/x,7Xl~KqPSFW;ZMe~k*-?g3~!xVt36doCn6hKO<h');
define('NONCE_SALT',       ' m]dP~SPdtj}E0esZ~uPd-4(03HzmDk48p5|RnGYb$3_o@b!3,XQT5k#-bh~0tzm');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
